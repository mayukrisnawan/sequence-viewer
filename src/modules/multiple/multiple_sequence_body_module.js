var MultipleSequenceBodyModule = {};

MultipleSequenceBodyModule.sequenceDescription = function(viewer, row, index) {
  var sequences = viewer.sequences();
  var sequence = sequences.find(row);
  var code = sequence.body()[index];
  if (code == "?") return "";
  if (code == "*") {
    var nucleotide = sequence.nucleotide();
    code = nucleotide.substr(index*3, 3);
  }
  if (sequence.inProteinMode()) {
    return proteinName[code];  
  } else {
    return nucleotideName[code];
  }
}

MultipleSequenceBodyModule.bindSequenceBodyEventsTo = function(viewer) {
  var em = viewer.elementManager();

  em.attach("sequence_box", ".sv_sequence_box");
  em.attach("sequence_label", ".sv_sequence_label");
  em.attach("consensus_box", ".sv_consensus_box");
  em.attach("consensus_label", ".sv_consensus_label");

  // Register sequence box events
  em.event("sequence_box", "mouseover", events["sequence_body"].box_hover);
  em.event("sequence_box", "mouseout", events["sequence_body"].box_out);
  // em.event("sequence_box", "click", events["sequence_selection"].box_click);

  // Register sequence label events
  em.event("sequence_label", "mouseover", events["sequence_body"].label_hover);
  em.event("sequence_label", "mouseout", events["sequence_body"].label_out);
  // em.event("sequence_label", "click", events["sequence_selection"].label_click);

  // Register sequence box events
  em.event("consensus_box", "mouseover", events["sequence_body"].box_hover);
  em.event("consensus_box", "mouseout", events["sequence_body"].box_out);

  // Register sequence label events
  em.event("consensus_label", "mouseover", events["sequence_body"].consensus_label_hover);
  em.event("consensus_label", "mouseout", events["sequence_body"].consensus_label_out);
}

MultipleSequenceBodyModule.loadingState = function(viewer, $container) {
  var $parent = $container.parent();
  $parent.append("\
  <div class='msv_load_state'>\
    Loading..\
  </div>\
  ");

  var em = viewer.elementManager();
  em.attach("load_state", ".msv_load_state");

  viewer.elementManager().$base().find(".msv_consensus")
        .css("overflow-x", "")
        .html("");

  var height = $parent.parent().parent().height();
  $parent.attr("ori-height", $parent.height()).height(height);

  var $loadState = viewer.$el("load_state");
  $loadState.css("padding", (height/2-5) + "px 0px 0px 0px");

  $container.css("display", "none")
  $parent.css("overflow", "hidden");
}

MultipleSequenceBodyModule.notifyLoading = function(viewer, $container, percent) {
  var $loadState = viewer.$el("load_state");
  if (percent === undefined) {
    $loadState.html("Loading..");
  } else {
    $loadState.html("Loading " + percent + "%");
  }
}

MultipleSequenceBodyModule.normalState = function(viewer, $container) {
  var $loadState = viewer.$el("load_state");
  $loadState.remove();
  $container.parent()
    .height($container.parent().attr("ori-height"))
    .removeAttr("ori-height");
  $container.fadeIn(500);
}

MultipleSequenceBodyModule.loadSequenceBodySVG = function(viewer, $container, sequences, boxSize, callback) {
  var width = $container.width();
  $container.css("max-width", width);
  var $loadingContainer = $container;

  var bodyContainer = d3.select($container.get(0)).text("");

  row = 0;
  function loadSequence(){
    if (row == sequences.count()) {
      MultipleSequenceBodyModule.loadConsensus(viewer, boxSize);
      MultipleSequenceBodyModule.bindSequenceBodyEventsTo(viewer);
      MultipleSequenceBodyModule.normalState(viewer, $loadingContainer);
      if (callback !== undefined) callback();
      return;
    }
    MultipleSequenceBodyModule.notifyLoading(viewer, $loadingContainer, Math.round((row+1)/sequences.count() * 100));

    var sequenceSVG = bodyContainer.append("svg")
                      .attr("class", "sv_sequence_body_svg")
                      .attr("data-sequence-index", row)
                      .attr("width", (boxSize * sequences.find(row).body().length)+"px")
                      .attr("height", boxSize);

    var featureSVG  = bodyContainer.append("svg")
                                   .attr("class", "msv_feature_svg")
                                   .attr("data-sequence-index", row)
                                   .attr("height", 0);

    viewer.elementManager().attach("feature_svg_" + row, ".msv_feature_svg[data-sequence-index='" + row + "']");

    var sequence = sequences.find(row);

    // sequence box
    sequenceSVG.selectAll("rect.msv_sequence_box_" + row)
      .data(sequence.body())
      .enter()
      .append("rect")
        .attr("class", function(d,j){ 
          var classes = "msv_sequence_box msv_sequence_box_ " + row;
          classes += " sv_sequence_box sv_sequence_box_" + j;
          return classes; 
        })
        .attr("data-row-index", row)
        .attr("data-sequence-index", function(d,j){ return j; })
        .attr("data-highlighted", "false")
        .attr("x", function(d,j){ return j * boxSize; })
        //.attr("y", row * boxSize)
        .attr("width", boxSize)
        .attr("height", boxSize)
        .style("fill", function(d,i){ return SequenceBodyModule.sequenceColor(d); })
        .attr("ori-color", function(d,i){ return SequenceBodyModule.sequenceColor(d); })
        .attr("stroke", "#000");

    // sequence label
    sequenceSVG.selectAll(".msv_sequence_label_" + row)
      .data(sequence.body())
      .enter()
        .append("text")
          .attr("class", function(d,j){ 
            var classes = "msv_sequence_label msv_sequence_label_ " + row;
            classes += " sv_sequence_label sv_sequence_label_" + j;
            return classes; 
          })
          .attr("data-row-index", row)
          .attr("data-sequence-index", function(d,j){ return j; })
          .attr("data-highlighted", "false")
          .attr("ori-color", function(d,i){ return SequenceBodyModule.sequenceColor(d); })
          .attr("transform", function(d,j) {
            var x = j * boxSize + (boxSize-10)/2;
            //var y = boxSize * row;
            var y = 0;
            y = y + 10 + ((boxSize-10)/2);
            return "translate ("+ x + "," + y + ")";
          })
          .each(function(d,i){
            d3.select(this).text(d);
          });
    if (row == sequences.count()-1) MultipleSequenceBodyModule.notifyLoading(viewer, $loadingContainer, 100);
    row++;      
    setTimeout(loadSequence, 200);
  }

  MultipleSequenceBodyModule.loadingState(viewer, $loadingContainer);
  FeatureManagerModule.disable(viewer);
  loadSequence();
}

MultipleSequenceBodyModule.loadConsensus = function(viewer, boxSize){
  var consensus = viewer.sequences().consensus();
  var $target = viewer.elementManager().$base();
  var $container = $target.find(".msv_consensus");

  var width = $container.width();
  $container.css("max-width", width).css("overflow-x", "scroll");

  $container.scroll(function(e){
    $target.find(".sv_sequence_body").scrollLeft($container.scrollLeft());
  });

  $target.find(".sv_sequence_body").scroll(function(e){
    $container.scrollLeft($target.find(".sv_sequence_body").scrollLeft());
  });

  var bodyContainer = d3.select($container.get(0)).text("")
    .append("svg")
    .attr("class", "sv_consensus_svg")
    .attr("width", (boxSize * consensus.length)+"px")
    .attr("height", boxSize);

  var consensusAnnotation = function(code, index){
    var msg = "Site #" + (index+1);
    if (code == "-" || code == "?" || code == "*") return msg;
    if (viewer.inProteinMode()) {
      msg += " " + proteinName[code];  
    } else {
      msg += " " + nucleotideName[code];
    }
    return msg;
  }

  // sequence box
  bodyContainer.selectAll("rect")
    .data(consensus)
    .enter()
    .append("rect")
    .attr("class", "sv_consensus_box")
      .attr("data-sequence-index", function(d,j){ return j; })
      .attr("consensus-annotation", consensusAnnotation)
      .attr("data-highlighted", "false")
      .attr("x", function(d,j){ return j * boxSize; })
      .attr("y", 0)
      .attr("width", boxSize)
      .attr("height", boxSize)
      .style("fill", function(d,i){ return SequenceBodyModule.sequenceColor(d); })
      .attr("ori-color", function(d,i){ return SequenceBodyModule.sequenceColor(d); })
      .attr("stroke", "#000");
  
  // sequence label
  bodyContainer.selectAll(".sv_consensus_label")
    .data(consensus)
    .enter()
      .append("text")
        .attr("class", "sv_consensus_label")
        .attr("data-sequence-index", function(d,j){ return j; })
        .attr("consensus-annotation", consensusAnnotation)
        .attr("data-highlighted", "false")
        .attr("ori-color", function(d,i){ return SequenceBodyModule.sequenceColor(d); })
        .attr("transform", function(d,j) {
          var x = j * boxSize + (boxSize-10)/2;
          var y = 10 + ((boxSize-10)/2);
          return "translate ("+ x + "," + y + ")";
        })
        .each(function(d,i){
          d3.select(this).text(d);
        });
}