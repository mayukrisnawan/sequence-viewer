MultipleSequenceFeatureModule = {};

MultipleSequenceFeatureModule.renderFeatureTo = function(viewer, callback){
  var featureManager = viewer.featureManager(),
      sequences = viewer.sequences(),
      featureBoxSize = viewer.configs("feature_box_size");

  var sequenceIndex = 0;
  var boxSize = viewer.configs("sequence_box_size"),
      featureBoxSize = viewer.configs("feature_box_size");

  var layersList = [];

  var renderFeatures = function(){
    var sequence = sequences.find(sequenceIndex);
    var layers = featureManager.layers(sequence);
    layersList.push(layers);
    var $svg = viewer.$el("feature_svg_" + sequenceIndex);
    var svg = d3.select($svg.get(0));

    SequenceFeatureModule.createFeatureLayers(viewer, svg, layers, boxSize, featureBoxSize, function(){
      var $spacer = viewer.elementManager().$base().find(".msv_feature_spacer[data-sequence-index='" + sequenceIndex + "']");
      if (layers.length == 0) {
        $spacer.css("display", "none");
      } else {
        $spacer.css("display", "").height(featureBoxSize * layers.length);
      }
      sequenceIndex++;
      var percent = sequenceIndex/sequences.count() * 100;
      percent = Math.round(percent);
      viewer.notifyLeft("Loading features " + percent + "%");
      if (sequenceIndex == sequences.count() || !viewer.processRunning("render_features")) {
        SequenceFeatureModule.bindSequenceFeaturesEventsTo(viewer);
        SingleSVModules.setContainerDimension(viewer);
        viewer.terminateProcess("render_features");
        viewer.attachProcess("global_bind_feature_index", renderFeatures);
        bindFeatureIndex();
      } else {
        renderFeatures();
      }
    }, sequenceIndex);
  }

  var layerIndex = 0;
  var bindFeatureIndex = function(){
    SequenceFeatureModule.bindFeatureIndex(viewer, layersList[layerIndex], layerIndex, layersList.length, function(){
      layerIndex++;
      if (layerIndex == layersList.length || !viewer.processRunning("global_bind_feature_index")) {
        FeatureManagerModule.renderSequenceBoxes(viewer, layersList[layerIndex]);
        FeatureManagerModule.enable(viewer);
        viewer.terminateProcess("global_bind_feature_index");
        if (typeof callback == "function") callback();
      } else {
        bindFeatureIndex();
      }
    });
  }

  FeatureManagerModule.disable(viewer);
  viewer.attachProcess("render_features", renderFeatures);
  renderFeatures();
}