var MultipleSVModules = {};

/*
 * Render container to viewer element
 * @args viewer MultipleSequeneViewer
 * @args $target jQuerySelector
 */
MultipleSVModules.createContainer = function(viewer, $target){
  SingleSVModules.createContainer(viewer, $target);
  viewer.elementManager().$base()
    .find(".svsc_btn_show_sc_table, .svae_btn_show, .sv_protein_preview_bar, .sv_sequence_selection_mode, .sv_smallbar").remove();
}

/*
 * Register user interface element handler
 * @args viewer SingleSequeneViewer
 */
MultipleSVModules.registerElements = function(viewer){
  SingleSVModules.registerElements(viewer);
  var em = viewer.elementManager();
  em.attach("row_input", ".sv_row_input");
  em.attach("sequence_header.topbar", ".msv_sequence_header_topbar");
  em.attach("sequence_header.search_input", ".msv_search_header_input");
  em.attach("toolbar.alignment", ".msv_alignment");
  em.attach("toolbar.tree", ".msv_tree");
  viewer.$el("row_input").parent().show();
}

/*
 * Register viewer event handlers
 * @args viewer MultipleSequeneViewer
 */
MultipleSVModules.registerEvents = function(viewer){
  SingleSVModules.registerEvents(viewer);
  var em = viewer.elementManager();
  em.event("row_input", "keyup", events["row_input"].update_row_input);
  em.event("row_input", "change", events["row_input"].update_row_input);
  em.event("sequence_header.search_input", "keyup", events["sequence_header.search_input"]);
  em.event("toolbar.alignment", "click", function(){
    viewer.alignSequences();
  });
  em.event("toolbar.tree", "click", function(){
    viewer.showTree();
  });
}

