var SequenceBodyModule = {};

SequenceBodyModule.sequenceDescription = function(viewer, index) {
  var sequence = viewer.sequence();
  var code = sequence.body()[index];
  if (code == "?") return "";
  if (code == "*") {
    var nucleotide = sequence.nucleotide();
    code = nucleotide.substr(index*3, 3);
  }
  if (sequence.inProteinMode()) {
    if (proteinName[code] !== undefined) return proteinName[code]; 
  } else {
    var ambiguityRegex = /[KMRYSWBVHDXN]/g;
    if (ambiguityRegex.exec(code) != null) return "Ambiguity";
    if (nucleotideName[code] !== undefined) return nucleotideName[code];
  }
  return "";
}

SequenceBodyModule.sequenceColor = function(code) {
  if (sequenceColors[code]) return sequenceColors[code];
  return "#eee";
}

SequenceBodyModule.bindSequenceBodyEventsTo = function(viewer) {
  var em = viewer.elementManager();
  // Register sequence box events
  em.attach("sequence_box", ".sv_sequence_box");
  em.attach("sequence_label", ".sv_sequence_label");
  em.event("sequence_box", "mouseover", events["sequence_body"].box_hover);
  em.event("sequence_box", "mouseout", events["sequence_body"].box_out);
  em.event("sequence_box", "click", events["sequence_selection"].box_click);

  // Register sequence label events
  em.event("sequence_label", "mouseover", events["sequence_body"].label_hover);
  em.event("sequence_label", "mouseout", events["sequence_body"].label_out);
  em.event("sequence_label", "click", events["sequence_selection"].label_click);
}

SequenceBodyModule.loadProteinPreview = function(viewer, $container, sequence, boxSize) {
  FeatureManagerModule.disable(viewer);
  var width = $container.width();
  $container.css("max-width", width).css("overflow-x", "scroll");

  var container = d3.select($container.get(0)).text("")
  var proteins = sequence.computeProteinPreview();

  var proteinAnnotation = function(code){
    if (code == "-" || code == "?" || code == "*") return "";
    if (proteinName[code] !== undefined) return proteinName[code];
    return "";
  }

  var findSequenceBoxes = function(proteinIndex, layerIndex){
    var $container = viewer.$el("sequence_body");
    var startIndex = proteinIndex*3 + layerIndex;
    var expr = ".sv_sequence_box[data-sequence-index='" + (startIndex) + "']";
        expr += ", .sv_sequence_box[data-sequence-index='" + (startIndex + 1) + "']";
        expr += ", .sv_sequence_box[data-sequence-index='" + (startIndex + 2) + "']";
    var $boxes = $container.find(expr);
    return $boxes;
  }

  var hoverSequences = function(proteinIndex, layerIndex){
    var $boxes = findSequenceBoxes(proteinIndex, layerIndex);
    $boxes.mouseover();
  }

  var unhoverSequences = function(proteinIndex, layerIndex){
    var $boxes = findSequenceBoxes(proteinIndex, layerIndex);
    $boxes.mouseout();
  }

  var boxMouseHover = function(code, index){
    return function(){
      var msg = proteinAnnotation(code);
      this.style.fill = "rgb(255, 238, 48)";
      var layerIndex = $(this).attr("protein-layer-index");
      layerIndex = parseInt(layerIndex);
      hoverSequences(index, layerIndex);
      viewer.notifyRight(msg);
    }
  }

  var boxMouseOut = function(code, index){
    return function(){
      this.style.fill = "rgb(238, 238, 238)";
      var layerIndex = $(this).attr("protein-layer-index");
      layerIndex = parseInt(layerIndex);
      unhoverSequences(index, layerIndex);
      viewer.notifyRight("");
    }
  }

  var labelMouseHover = function(code, index){
    return function(){
      var layerIndex = $(this).attr("protein-layer-index");
      layerIndex = parseInt(layerIndex);
      var $box = viewer.elementManager().$base()
                  .find(".sv_protein_preview_svg > rect[protein-index='" + index + "'][protein-layer-index='" + layerIndex + "']");
      $box.mouseover();
    }
  }

  var labelMouseOut = function(code, index){
    return function(){
      var layerIndex = $(this).attr("protein-layer-index");
      layerIndex = parseInt(layerIndex);
      var $box = viewer.elementManager().$base()
                  .find(".sv_protein_preview_svg > rect[protein-index='" + index + "'][protein-layer-index='" + layerIndex + "']");
      $box.mouseout();
    }
  }

  for (var i=0; i<proteins.length; i++) {
    var protein = proteins[i];
    var bodyContainer = container.append("svg")
      .attr("class", "sv_protein_preview_svg")
      .attr("width", (boxSize * 3 * protein.length + i * boxSize)+"px")
      .attr("height", boxSize);

    // sequence box
    bodyContainer.selectAll("rect")
      .data(protein)
      .enter()
      .append("rect")
        .property("onmouseover", boxMouseHover)
        .property("onmouseout", boxMouseOut)
        .attr("protein-layer-index", i)
        .attr("protein-index", function(d,j){ return j; })
        .style("cursor", "pointer")
        .attr("x", function(d,j){ return (j * boxSize  * 3 + i * boxSize); })
        .attr("y", 0)
        .attr("width", boxSize * 3)
        .attr("height", boxSize)
        .style("fill", "#eee")
        .attr("stroke", "#000");
    
    // sequence label
    bodyContainer.selectAll(".sv_protein_preview_label")
      .data(protein)
      .enter()
        .append("text")
          .property("onmouseover", labelMouseHover)
          .property("onmouseout", labelMouseOut)
          .attr("protein-layer-index", i)
          .attr("protein-index", function(d,j){ return j; })
          .style("cursor", "pointer")
          .attr("class", function(d,j){ return "sv_protein_preview_label sv_protein_preview_label_"+j})
          .attr("transform", function(d,j) {
            var x = j * boxSize * 3 + (boxSize-10)/2;
            var y = 10 + ((boxSize-10)/2);
            x += i * boxSize;
            return "translate ("+ x + "," + y + ")";
          })
          .each(function(d,i){
            d3.select(this).text(d);
          });
  }

  var $target = viewer.elementManager().$base();

  $container.scroll(function(e){
    var offset = $container.scrollLeft();
    $target.find(".sv_sequence_body").scrollLeft(offset);
  });

  $target.find(".sv_sequence_body").scroll(function(e){
    var offset = $target.find(".sv_sequence_body").scrollLeft();
    $container.scrollLeft(offset);
  });
}

SequenceBodyModule.loadSequenceBodySVG = function(viewer, $container, sequence, boxSize) {
  FeatureManagerModule.disable(viewer);
  var width = $container.width();
  $container.css("max-width", width);

  var bodyContainer = d3.select($container.get(0)).text("")
    .append("svg")
    .attr("class", "sv_sequence_body_svg")
    .attr("width", (boxSize * sequence.body().length)+"px")
    .attr("height", boxSize);

  // sequence box
  bodyContainer.selectAll("rect")
    .data(sequence.body())
    .enter()
    .append("rect")
      .attr("class", function(d,j){ return "sv_sequence_box sv_sequence_box_" + j; })
      .attr("data-sequence-index", function(d,j){ return j; })
      .attr("data-highlighted", "false")
      .attr("x", function(d,j){ return j * boxSize; })
      .attr("y", 0)
      .attr("width", boxSize)
      .attr("height", boxSize)
      .style("fill", function(d,i){ return SequenceBodyModule.sequenceColor(d); })
      .attr("ori-color", function(d,i){ return SequenceBodyModule.sequenceColor(d); })
      .attr("stroke", "#000");
  
  // sequence label
  bodyContainer.selectAll(".sv_sequence_label")
    .data(sequence.body())
    .enter()
      .append("text")
        .attr("class", function(d,j){ return "sv_sequence_label sv_sequence_label_"+j})
        .attr("data-sequence-index", function(d,j){ return j; })
        .attr("data-highlighted", "false")
        .attr("ori-color", function(d,i){ return SequenceBodyModule.sequenceColor(d); })
        .attr("transform", function(d,j) {
          var x = j * boxSize + (boxSize-10)/2;
          var y = 10 + ((boxSize-10)/2);
          return "translate ("+ x + "," + y + ")";
        })
        .each(function(d,i){
          d3.select(this).text(d);
        });

  // bind sequence body events to viewer
  SequenceBodyModule.bindSequenceBodyEventsTo(viewer);
}

SequenceBodyModule.loadRuler = function(viewer, $container, sequence) {
  var boxSize = viewer.configs("sequence_box_size"),
      length = sequence.body().length;
  var sequenceScale = d3.scale.linear()
                        .domain([1, length+1])
                        .range([0, boxSize * length])

  var axis = d3.svg.axis()
              .scale(sequenceScale)
              .ticks(Math.round(length/5))
  
  var ruler = d3.select($container.get(0))
    .insert("svg", ".sv_sequence_body_svg")
    .attr("class", "sv_ruler")
    .attr("width", (boxSize * (length-1) + 20)+"px")
    .attr("height", "22px")
      .append("g")
        .attr("transform", "translate(0, -5)")
        .transition().duration(600)
        .call(axis);

  ruler.selectAll("text")
  .attr("font-size", "6px")
        .style("text-anchor", "start")
        .style("fill", "#555");

  ruler.selectAll("path")
        .style("fill", "#aaa")

  d3.select($container.get(0))
    .insert("svg", ".sv_sequence_body_svg")
    .attr("width", (boxSize * (length-1) + 20)+"px")
      .attr("height", "8px")
      .append("g")
        .attr("transform", "translate(0, 0)")
        .append("line")
          .attr("x2", boxSize * length)
          .attr("stroke", "#aaa")
          .attr("stroke-dasharray", "5")
}