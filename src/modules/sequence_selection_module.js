var SequenceSelectionModule = {};

SequenceSelectionModule.existOnSeletedSites = function(viewer, sequenceIndex, rowIndex){
	var selectedSites = viewer.selectedSites();
  for (var i=0; i<selectedSites.length; i++) {
    var selectedSite = selectedSites[i];
    if (multi(viewer)) {
      if (selectedSite.sequenceIndex == sequenceIndex && selectedSite.rowIndex == rowIndex) return true;
    } else {
      if (selectedSite.sequenceIndex == sequenceIndex) return true;
    }
  }
  return false;
}

SequenceSelectionModule.findNearestSelectedSites = function(viewer, sequenceIndex, rowIndex){
	var selectedSites = viewer.selectedSites();
  selectedSites.sort(function(a,b){
    return a.sequenceIndex-b.sequenceIndex;
  });
  // console.log(selectedSites);
  var result = {
    start: false,
    finish: false
  }

  for (var i=0; i<selectedSites.length; i++) {
    var selectedSite = selectedSites[i];
    if (multi(viewer) && selectedSite.rowIndex != rowIndex) continue;
    if (selectedSite.sequenceIndex < sequenceIndex) {
      result.start = selectedSite.sequenceIndex;
      // console.log("start : " + result.start);
    }
    if (selectedSite.sequenceIndex > sequenceIndex) {
      if (result.finish !== false) continue;
      result.finish = selectedSite.sequenceIndex;
      // console.log("finish : " + result.finish)
    }
  }
  return result;
}

SequenceSelectionModule.selectSite = function(viewer, $box){
  var sequenceIndex = parseInt($box.attr("data-sequence-index"));

  if (multi(viewer)) {
    var rowIndex = parseInt($box.attr("data-row-index"));
    if (SequenceSelectionModule.existOnSeletedSites(viewer, sequenceIndex, rowIndex)) return false;
    var nearestSelectSites = SequenceSelectionModule.findNearestSelectedSites(viewer, sequenceIndex, rowIndex);
  } else {
    if (SequenceSelectionModule.existOnSeletedSites(viewer, sequenceIndex)) return false;
    var nearestSelectSites = SequenceSelectionModule.findNearestSelectedSites(viewer, sequenceIndex);
  }


  if (viewer.selectionMode() == "single" || (nearestSelectSites.start === false && nearestSelectSites.finish === false) ) {
    if (multi(viewer)) {
      viewer.selectedSites().push({
        sequenceIndex: parseInt(sequenceIndex),
        rowIndex: parseInt(rowIndex)
      });
    } else {
      viewer.selectedSites().push({
        sequenceIndex: parseInt(sequenceIndex)
      });
    }
    $box.attr("data-selected", "true")
        .css("fill", "#f00")
    return;
  }

  if (nearestSelectSites.start !== false) {
    var start = nearestSelectSites.start;
    var finish = sequenceIndex;
  } else if (nearestSelectSites.finish !== false) {
    var start = sequenceIndex;
    var finish = nearestSelectSites.finish;
  }

  var expression = "";
  // console.log(start);
  // console.log(finish);
  for (var i=start; i<=finish; i++) {
    if (multi(viewer)) {
      if (SequenceSelectionModule.existOnSeletedSites(viewer, i, rowIndex)) continue;
    } else {
      if (SequenceSelectionModule.existOnSeletedSites(viewer, i)) continue;
    }
    if (multi(viewer)) {
      viewer.selectedSites().push({
        sequenceIndex: i,
        rowIndex: parseInt(rowIndex)
      });
    } else {
      viewer.selectedSites().push({
        sequenceIndex: i
      });
    }
    if (expression != "") expression += ",";
    expression += ".sv_sequence_box[data-sequence-index='" + i + "']";

    if (multi(viewer)) {
      expression += "[data-row-index='" + rowIndex + "']";
    }
  }

  var $boxes = viewer.$el("sequence_body").find(expression);
  $boxes.attr("data-selected", "true")
        .css("fill", "#f00")
}

SequenceSelectionModule.unselectSite = function(viewer, $box){
  var sequenceIndex = parseInt($box.attr("data-sequence-index"));
  if (multi(viewer)) {
    var rowIndex = parseInt($box.attr("data-row-index"));
  }
  for (var i=0; i<viewer.selectedSites().length; i++) {
    var selectedSite = viewer.selectedSites()[i];
    if (multi(viewer)) {
      if (selectedSite.sequenceIndex == sequenceIndex && selectedSite.rowIndex == rowIndex) viewer.selectedSites().splice(i, 1);
    } else {
      if (selectedSite.sequenceIndex == sequenceIndex) viewer.selectedSites().splice(i, 1);
    }
  }

  $box.attr("data-selected", "false")
      .css("fill", $box.attr("ori-color"));
}

SequenceSelectionModule.notifySelectedSites = function(viewer) {
  var msg = "";
  var count = viewer.selectedSites().length;
  if (count == 1) {
    msg = "one site selected";
  } else if (count != 0) {
    msg = count + " sites selected";
  }

  if (count != 0) {
    msg += ", \
      <a class='sv_clear_selection' style='cursor: pointer'>clear selection</a> | \
      <a class='sv_delete_selected_sites' style='cursor: pointer'>delete selected " + (count == 1 ? "site" : "sites") + "</a> \
    ";
  }
  viewer.$el("toolbar.sites_selection_display").html(msg);
}

SequenceSelectionModule.deleteSelectedSites = function(viewer){
  var selectedSites = viewer.selectedSites();
  selectedSites.sort(function(a,b){
    return a.sequenceIndex - b.sequenceIndex;
  });

  var target;
  if (multi(viewer)) {
    target = viewer.sequences()
  } else {
    target = viewer.sequence();
  }

  target.remove(selectedSites, function(){
    if (!viewer.inProteinMode()) {
      Ambiguity.check(viewer);
      if (single(viewer)) SequenceQualityModule.validate(viewer, target);
    }
    viewer.softReload();
    alert(selectedSites.length + " " + (selectedSites.length == 1 ? "site" : "sites") + " successfully deleted from " + (multi(viewer) ? "sequences." : "sequence.") );
  })
}