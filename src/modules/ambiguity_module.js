Ambiguity = {};
Ambiguity.registerElements = function(viewer){
  var em = viewer.elementManager();

  em.attach("ambiguity_editor", ".svae_container");
  em.attach("ambiguity_list", ".svae_list");
  em.attach("btn_show_ambiguity_editor", ".svae_btn_show");
  em.attach("btn_hide_ambiguity_editor", ".svae_btn_hide");
  // em.attach("btn_save_ambiguity", ".svae_btn_save");
  em.attach("ambiguity_count", ".svae_count");
}

Ambiguity.registerEvents = function(viewer){
  var em = viewer.elementManager();
  em.event("btn_show_ambiguity_editor", "click", events["sequence_ambiguity"].show);
  em.event("btn_hide_ambiguity_editor", "click", events["sequence_ambiguity"].hide);
  // em.event("btn_save_ambiguity", "click", function(){
  //   viewer.saveAmbiguity();
  // })
}

// source : http://www.boekhoff.info/?pid=data&dat=fasta-codes
Ambiguity.table = {
  "K":["G", "T"],
  "M":["A", "C"],
  "R":["A", "G"],
  "Y":["C", "T"],
  "S":["C", "G"],
  "W":["A", "T"],
  "B":["C", "G", "T"],
  "V":["A", "C", "G"],
  "H":["A", "C", "T"],
  "D":["A", "G", "T"],
  "X":["A", "C", "G", "T"],
  "N":["A", "C", "G", "T"],
  ".":[],
};

Ambiguity.fillAmbiguityEditor = function(viewer, ambiguities) {
  var $list = viewer.$el("ambiguity_list"),
      $count = viewer.$el("ambiguity_count");
      // $btn = viewer.$el("btn_save_ambiguity");

  if (ambiguities.length == 0) {
    $list.html("\
    <tr> \
      <td colspan='4'><center><i>There is no ambiguity in this sequence</i></center></td> \
    </tr> \
    ");
    $count.html("");
    // $btn.hide();
    return;
  }

  $list.html("");
  $count.html(" (" + ambiguities.length + ")");
  // $btn.show();
  
  Util.iterate(ambiguities, function(ambiguity, index){
    var replacement = "<select class='svae_replacement_select' data-position-index='" +  ambiguity.index + "'>";
    replacement += "<option value='" + ambiguity.code + "'>?</option>";
    var replacementCodes = Ambiguity.table[ambiguity.code];
    for (var i=0; i<replacementCodes.length; i++) {
      var replacementCode = replacementCodes[i];
      replacement += "<option value='" + replacementCode + "'>" + replacementCode + "</option>";
    }
    replacement += "</select>";

    $list.append("\
    <tr> \
      <td>" + (index+1) + "</td> \
      <td>" + ambiguity.code + "</td> \
      <td><a class='svae_position' href='" + ambiguity.index + "'>" + (ambiguity.index+1) + "</a></td> \
      <td>" + replacement + "</td> \
    </tr> \
    ");
  });

  // Slide to specified position
  viewer.elementManager().$base().find(".svae_position").click(function(){
    var index = $(this).attr("href");
    viewer.slideTo(Util.number(index));
    return false;
  });

  // Replacement code selected
  viewer.elementManager().$base().find(".svae_replacement_select").change(function(){
    var index = $(this).attr("data-position-index");
    viewer.slideTo(Util.number(index));
    var code = $(this).val();
    viewer.ambiguityCorrection({
      index: Util.number(index),
      code: code
    });
  });
}

Ambiguity.check = function(viewer){
  if (viewer.inProteinMode()) return false;
  var sequence = viewer.sequence(),
      body = sequence.body();
  var matcher = /[KMRYSWBVHDXN]/g;
  var ambiguities = [];

  var result;
  do {
    result = matcher.exec(body);
    if (result !== null) {
      ambiguities.push({
        code: result[0],
        index: result.index
      });
    }
  } while (result !== null);
  
  Ambiguity.fillAmbiguityEditor(viewer, ambiguities);
}