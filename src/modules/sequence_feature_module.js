SequenceFeatureModule = {};

SequenceFeatureModule.createColor = function(index){
  index = index % featureColors.length;
  return featureColors[index];
}

SequenceFeatureModule.bindSequenceFeaturesEventsTo = function(viewer, featureLayers) {
  var em = viewer.elementManager();
  
  em.attach("feature_layers", ".svf_layer");
  em.attach("feature_box", ".svf_box");
  em.attach("feature_label", ".svf_label");

  em.event("feature_box", "mouseover", events["sequence_feature"].box_hover, { featureLayers : featureLayers });
  em.event("feature_box", "mouseout", events["sequence_feature"].box_out);
  em.event("feature_label", "mouseover", events["sequence_feature"].label_hover, { featureLayers : featureLayers });
  em.event("feature_label", "mouseout", events["sequence_feature"].label_out);
}

SequenceFeatureModule.createFeatureLayers = function(viewer, svg, layers, boxSize, featureBoxSize, callback, row) {
  var max = null;
  var index = 0;
  for (var i = 0; i < layers.length; i++) {
    var layer = layers[i];
    for (var j = 0; j < layer.length; j++) {
      var feature = layer[j];
      if (feature.to > max || max === null) max = feature.to;
      feature.color = SequenceFeatureModule.createColor(index);
      index++;
    }
  }
  if (max === null) max = 0;

  $(svg[0]).html("")
  if (layers.length == 0) {
    svg.style("display", "none");
  } else {
    svg.attr("height", layers.length * featureBoxSize)
       .attr("width", (max + 1) * boxSize)
       .style("display", "");
  }

  // Draw feature to the screen
  Util.iterate(layers, function(layer, index){
    var layerContainer = svg.append("g")
      .attr("class", "svf_layer svf_layer_" + index)
      .attr("transform", "translate(0, " + (index * featureBoxSize)  + ")");

    // console.log(row);
    // console.log(layer);

    // feature box
    layerContainer.selectAll(".svf_box_" + index)
      .data(layer)
      .enter()
      .append("rect")
        .attr("data-feature-index", function(feature){ return feature.index })
        .attr("data-layer-index", index)
        .attr("data-row-index", row === undefined ? 0 : row)
        .attr("class", function(feature){ return "svf_box svf_box_" + index + "_" + feature.index })
        .attr("x", function(feature){ return feature.from * boxSize })
        .attr("width", function(feature){ return (feature.to - feature.from + 1) * boxSize })
        .attr("height", featureBoxSize)
        .attr("stroke", "#eee")
        .style("fill", function(feature){ return feature.color })
        .attr("ori-color", function(feature){ return feature.color })
        .attr("data-feature-name", function(feature){ return feature.name; })
        .attr("data-feature-type", function(feature){ return feature.type; })
        .attr("data-feature-from", function(feature){ return feature.from; })
        .attr("data-feature-to", function(feature){ return feature.to; });

    // feature label
    layerContainer.selectAll(".svf_label_" + index)
      .data(layer)
      .enter()
      .append("text")
        .text(function(feature){
          return feature.name;
        })
        .attr("data-feature-index", function(feature){ return feature.index })
        .attr("data-layer-index", index)
        .attr("data-row-index", row === undefined ? 0 : row)
        .attr("class", function(feature){ return "svf_label svf_label_" + index + "_" + feature.index })
        .attr("font-size", "8px")
        .attr("x", function(feature) {
          var width = d3.select(this).style("width");
          width = Util.number(width);
          var x = feature.from * boxSize;
          var offset = (feature.to - feature.from + 1) * boxSize;
          offset = Math.round(offset/2);
          x += offset;
          return x;
        })
        .attr("transform", function(feature) {
          return "translate (0, 10)";
        })
        .attr("ori-color", function(feature){ return feature.color })
        .attr("data-feature-name", function(feature){ return feature.name; })
        .attr("data-feature-type", function(feature){ return feature.type; })
        .attr("data-feature-from", function(feature){ return feature.from; })
        .attr("data-feature-to", function(feature){ return feature.to; });
  });

  if (typeof callback == "function") callback();
}

SequenceFeatureModule.bindFeatureIndex = function(viewer, layers, layersIndex, layersCount, callback){
  var expressions = [];
  Util.iterate(layers, function(layer, index){
    for (var i=0; i<layer.length; i++) {
      var feature = layer[i];
      for (var j=feature.from; j<=feature.to; j++) {
        if (multi(viewer)) {
          var expression = ".sv_sequence_box[data-sequence-index='" + j + "']";
          expression += "[data-row-index='" + layersIndex + "']";
          expression += ", .sv_sequence_label[data-sequence-index='" + j + "']";
          expression += "[data-row-index='" + layersIndex + "']";
        } else {
          var expression = ".sv_sequence_box[data-sequence-index='" + j + "']";
          expression += ", .sv_sequence_label[data-sequence-index='" + j + "']";
        }
        expressions.push({
          expression:expression,
          featureIndex:feature.index
        });
      }
    }
  });

  do {
    var number = Math.round(Math.random() * 1000);
    var processName = "bind_feature_index" + number;
  } while(viewer.processRunning(processName));

  // async!!
  var expressionIndex = 0;
  var $loadingContainer = viewer.$el("sequence_body");
  var bindFeatureIndex = function(){
    if (expressions[expressionIndex] === undefined || !viewer.processRunning(processName)) {
      viewer.notifyLeft("");
      viewer.terminateProcess(processName);
      if (typeof callback == "function") callback();
      return;
    }
    FeatureManagerModule.disable(viewer);
    viewer.$el("sequence_body").find(expressions[expressionIndex].expression)
          .attr("data-feature-index", expressions[expressionIndex].featureIndex);
    var percent = (expressionIndex+1)/expressions.length * 100;
    if (multi(viewer)) {
      percent = (expressionIndex+1)/expressions.length * 100 / layersCount;
      percent += 100 / layersCount * layersIndex;
    }
    percent = Math.round(percent);
    viewer.notifyLeft("Loading feature manager " + percent + "%");
    setTimeout(function(){
      expressionIndex++;
      bindFeatureIndex();
    }, 1);
  }

  if (expressions.length == 0) {
    viewer.notifyLeft("");
    if (typeof callback == "function") callback();
    return;
  }

  $testElement = viewer.$el("sequence_body").find(expressions[expressionIndex].expression);
  if ($testElement && $testElement.attr("data-feature-index") === undefined) {
    viewer.attachProcess(processName, bindFeatureIndex);
    bindFeatureIndex();
  } else {
    viewer.notifyLeft("");
    if (typeof callback == "function") callback();
  }
}

SequenceFeatureModule.renderFeatureTo = function(viewer, callback){
  var featureManager = viewer.featureManager(),
      sequence = viewer.sequence();
  var container = viewer.dom("sequence_body");
  var $svg = viewer.$el("sequence_body").find(".svf_svg");

  // Transform input data
  var layers = featureManager.layers(sequence);  

  var boxSize = viewer.configs("sequence_box_size"),
      featureBoxSize = viewer.configs("feature_box_size");

  if ($svg.length  != 0) $svg.remove();
  var svg = d3.select(container)
              .append("svg")
              .attr("class", "svf_svg");

  FeatureManagerModule.disable(viewer);
  SequenceFeatureModule.createFeatureLayers(viewer, svg, layers, boxSize, featureBoxSize, function(){
    SequenceFeatureModule.bindSequenceFeaturesEventsTo(viewer);
    SequenceFeatureModule.bindFeatureIndex(viewer, layers, 0, 1, function(){
      FeatureManagerModule.renderSequenceBoxes(viewer);
      FeatureManagerModule.enable(viewer);
    });
    if (typeof callback == "function") callback();
  });
}