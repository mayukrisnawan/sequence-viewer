SequenceQualityModule = {};
SequenceQualityModule.registerElements = function(viewer){
  var em = viewer.elementManager();

  em.attach("sequence_quality", ".svsc_container");
  em.attach("sequence_quality_list", ".svsc_list");
  em.attach("btn_show_sequence_quality", ".svsc_btn_show_sc_table");
  em.attach("btn_hide_sequence_quality", ".svsc_btn_hide_sc_table");
  em.attach("sequence_quality_errors_count", ".svsc_errors_count");
}

SequenceQualityModule.registerEvents = function(viewer){
  var em = viewer.elementManager();
  em.event("btn_show_sequence_quality", "click", events["sequence_quality"].show);
  em.event("btn_hide_sequence_quality", "click", events["sequence_quality"].hide);
}

SequenceQualityModule.validate = function(viewer, sequence) {
  var featureManager = viewer.featureManager();
  var em = viewer.elementManager();
  var results = featureManager.validate(sequence);
  var $scList = viewer.$el("sequence_quality_list");

  if (results.length != 0) {
    viewer.notifyLeft("<font color='red'><i>This sequence has bad quality</i></font>, <a href='' class='svsc_show_details'>check sequence quality details..</a>");
    em.attach("sequence_quality_details", ".svsc_show_details");
    em.event("sequence_quality_details", "click", function(e){
      events["sequence_quality"].show(e);
      return false;
    });

    viewer.$el("sequence_quality_errors_count").html("(" + results.length + ")");
  } else {
    $scList.html("\
      <tr> \
        <td colspan='2'><center><i>No quality problems found</i></center></td> \
      </tr>\
    ");
    viewer.$el("sequence_quality_errors_count").html("");
    return;
  }

  $scList.html("\
    <tr> \
      <td colspan='2'><b>This sequence has bad quality because of the following reason :</b></td> \
    </tr>\
  ");

  
  for (var i=0; i<results.length; i++) {
    var result = results[i];
    $scList.append("\
    <tr> \
      <td width='2%'> " + (i+1) + "</td> \
      <td> " + result.message + "</td> \
    </tr> \
    ");
  }
}