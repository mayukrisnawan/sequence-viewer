var SingleSVModules = {};

/*
 * Render container to viewer element
 * @args viewer SingleSequeneViewer
 * @args $target jQuerySelector
 */
SingleSVModules.createContainer = function(viewer, $target){
  var tm = viewer.templateManager();
  tm.flush();
  tm.append(templates["topbar"]);
  tm.append(templates["toolbar"].open);
  tm.append(templates["feature_manager"]);
  tm.append(templates["sequence_quality"]);
  tm.append(templates["ambiguity_editor"]);
  tm.append(templates["toolbar"].close);
  tm.append(templates["smallbar"].open);
  tm.append(templates["smallbar"].close);
  // tm.append(templates["consensus_bar"]);
  tm.append(templates["midbar"]);
  tm.append(templates["footer"]);

  // Viewer wrapper
  tm.prepend(templates["viewer"].open);
  tm.append(templates["viewer"].close);

  $target.html(tm.contents());

  if (single(viewer)) {
    viewer.elementManager().$base().find(".msv_alignment, .msv_tree, .msv_consensus_bar").remove();
  }
}

/*
 * Register user interface element handler
 * @args viewer SingleSequeneViewer
 */
SingleSVModules.registerElements = function(viewer){
  var em = viewer.elementManager();
  //
  // TOPBAR
  //
  em.attach("site_input", ".sv_site_input");
  em.attach("search_input", ".sv_search_input");

  //
  // TOOLBAR
  //
  em.attach("toolbar.sequence_form", ".sv_sequence_form");
  em.attach("toolbar.sequence_selection_mode", ".sv_sequence_selection_mode");
  em.attach("toolbar.sites_selection_display", ".sv_sites_selection_display");
  em.attach("toolbar.zoom_slider", ".sv_zoom_slider");
  em.attach("toolbar.zoom_line", ".sv_zoom_line");

  // FEATURE MANAGER
  em.attach("feature_manager", ".svfm_container");
  em.attach("feature_table", ".svfm_feature_table");
  em.attach("feature_list", ".svfm_feature_list");
  em.attach("btn_show_feature_manager", ".svfm_btn_show_fm");
  em.attach("btn_hide_feature_manager", ".svfm_btn_hide_fm");
  em.attach("btn_toggle_features", ".svfm_toggle_features");
  
  //
  // MIDBAR
  //
  em.attach("sequence_header", ".sv_sequence_header");
  em.attach("sequence_body", ".sv_sequence_body");
  em.attach("protein_preview", ".sv_protein_preview");

  //
  // FOOTER
  //
  em.attach("left_status", ".sv_left_status");
  em.attach("right_status", ".sv_right_status");
}

/*
 * Register viewer event handlers
 * @args viewer SingleSequeneViewer
 */
SingleSVModules.registerEvents = function(viewer){
  var em = viewer.elementManager();
  //
  // ZOOM EVENTS
  //
  // em.event("toolbar.zoom_slider", "click", events["zoom_slider"].click);
  // em.event("toolbar.zoom_slider", "mousemove", events["zoom_slider"].mousemove);
  // em.event("toolbar.zoom_line", "mousemove", events["zoom_line"].mousemove);

  // FEATURE MANAGER EVENTS
  em.event("btn_show_feature_manager", "click", events["feature_manager"].show);
  em.event("btn_hide_feature_manager", "click", events["feature_manager"].hide);
  em.lateEvent("btn_toggle_features", "click", events["feature_manager"].toggle_feature);

  //
  // TOP BAR EVENTS
  //
  em.event("site_input", "keyup", events["site_input"].update_site_input);
  em.event("site_input", "change", events["site_input"].update_site_input);
  em.event("search_input", "keydown", events["search_input"].search_sequence);
  em.event("search_input", "keyup", events["search_input"].flush_search_input);

  // TOOLBAR EVENTS
  em.event("toolbar.sequence_form", "change", events["toolbar.sequence_form"].change);
  em.event("toolbar.sequence_selection_mode", "change", events["toolbar.sequence_selection_mode"].change);
  em.$base().on("click", ".sv_clear_selection", function(){
    viewer.clearSiteSelection();
    return false;
  });
  em.$base().on("click", ".sv_delete_selected_sites", function(){
    viewer.deleteSelectedSites();
    return false;
  });
}

/*
 * Unhighlight sequence
 * @args viewer SingleSequeneViewer
 */
SingleSVModules.unhighlightSequence = function(viewer){
  var $container = viewer.$el("sequence_body");
  var $sequenceBoxes = $container.find(".sv_sequence_box[data-highlighted='true']");
  $sequenceBoxes.each(function(){
    var $this = $(this),
        color = $this.attr("ori-color");
    $this.css("fill", color);
    $this.attr("data-highlighted", "false");
  });
}

/*
 * Highlight sequence
 * @args viewer SingleSequeneViewer
 * @args start Number
 * @args finish Number
 * @args row Number
 */
SingleSVModules.highlightSequence = function(viewer, start, finish, row){
  SingleSVModules.unhighlightSequence(viewer);
  var $container = viewer.$el("sequence_body");
  var selector = "",
      color = viewer.configs("highlight_color");
  for (var i=start; i<=finish; i++) {
    selector += ".sv_sequence_box_" + i;
      if (row != undefined) selector += "[data-row-index='" + row + "']";
    if (i != finish) selector += ", ";
  }
  // console.log(selector);
  var $sequenceBoxes = $container.find(selector);

  $sequenceBoxes.each(function(){
    var $this = $(this);
    $this.css("fill", color);
    $this.attr("data-highlighted", "true");
  });
  viewer.slideTo(start);
}

SingleSVModules.setContainerDimension = function(viewer){
  var $target = viewer.elementManager().$base();
  var $container = $target.find(".sv_container");

  if (multi(viewer)) {
    var headerHeight = $target.find(".msv_sequence_header_topbar").height();
    headerHeight =+ $target.find(".sv_sequence_header").height();
    $target.find(".sv_sequence_header_outer").parent().height(headerHeight);
  }

  var height = 0;
  height += $container.find(".sv_midbar").height();
  height += $container.find(".sv_topbar").height();
  height += $container.find(".sv_footer").height();
  height += $container.find(".sv_toolbar").height();
  height += 60;
  $container.height(height);
}