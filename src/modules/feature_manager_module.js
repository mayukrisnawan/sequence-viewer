FeatureManagerModule = {};

FeatureManagerModule.updateFeatureDisplay = function(viewer, callback){
  var em = viewer.elementManager(),
      fm = viewer.featureManager();
  em.$base().find(".svfm_feature_checkbox").each(function(){
    var index = $(this).attr("data-feature-index");

    var selectorExp = ".svf_box[data-feature-index='" + index + "']";
    selectorExp += ", .svf_label[data-feature-index='" + index + "']";
    var $featureBox = em.$base().find(selectorExp);

    if (this.checked) {
      fm.find(index).enable();
    } else {
      fm.find(index).disable();
    }
  });

  // if (fm.disabledCount() == 0) {
  //   viewer.showRuler();
  // } else {
  //   viewer.hideRuler();
  // }

  if (multi(viewer)) {
    MultipleSequenceFeatureModule.renderFeatureTo(viewer, callback);
  } else {
    SequenceFeatureModule.renderFeatureTo(viewer, callback);
  }
}

FeatureManagerModule.loadFeatureList = function(viewer){
  var $container = viewer.$el("feature_list");
  var $table = viewer.$el("feature_table");
  var features = viewer.featureManager().all();
  if (features.length == 0) {
    $container.html("<td colspan='6'><center><i>No features added</i></center></td>");
    return;
  }
  $table.find(".svfm_feature_checkbox_all").removeAttr("disabled");
  $container.html("");
  
  Util.iterate(features, function(feature, index){
    $container.append("\
      <tr> \
        <td style='margin:0; padding:0px 0px 0px 8px'> \
          <input type='checkbox' class='svfm_feature_checkbox' data-feature-index='" + index + "' checked/> \
        </td> \
        <td> " + (index+1) + "</td> \
        <td> " + feature.name() + "</td> \
        <td> " + feature.lengthExpression() + "</td> \
        <td> " + feature.rangeExpression().start + "</td> \
        <td> " + feature.rangeExpression().finish + "</td> \
        <td> " + feature.motifs().start + "</td> \
        <td> " + feature.motifs().finish + "</td> \
      </tr> \
    ");
  });
  FeatureManagerModule.registerEvents(viewer);
}

FeatureManagerModule.registerEvents = function(viewer){
  var em = viewer.elementManager();
  em.attach("feature_check_box_all", ".svfm_feature_checkbox_all");
  em.attach("feature_check_boxes", ".svfm_feature_checkbox");

  em.lateEvent("feature_check_box_all", "click", function(){
    var $checkboxes = em.get("feature_check_boxes").$el();
    if (this.checked) {
      $checkboxes.each(function(){
        this.checked = true;
      });
    } else {
      $checkboxes.removeAttr("checked");
    }
    FeatureManagerModule.updateFeatureDisplay(viewer);
  });

  em.lateEvent("feature_check_boxes", "click", function(){
    var $checkboxes = em.$base().find(".svfm_feature_checkbox:checked");
    if ($checkboxes.length == 0) {
      em.get("feature_check_box_all").dom().checked = false;
    }
    var fm = viewer.featureManager();
    if (fm.all().length == $checkboxes.length) {
      em.get("feature_check_box_all").dom().checked = true;
    }
    $(this).attr("disabled", "disabled");
    viewer.notifyRight("Updating features..");

    var $this = $(this);
    FeatureManagerModule.updateFeatureDisplay(viewer, function(){
      $this.removeAttr("disabled");
      viewer.notifyRight("");
    });
  });
}

FeatureManagerModule.renderSequenceBoxes = function(viewer){
  viewer.clearSiteSelection();
  var fm = viewer.featureManager();
  var $container = viewer.$el("sequence_body");
  var $svg = $container.find(".sv_sequence_body_svg");

  Util.iterate(fm.all(), function(feature, index){
    var $sequenceBoxes = $svg.find(".sv_sequence_box[data-feature-index='" + index + "']");
    var $sequenceLabels = $svg.find(".sv_sequence_label[data-feature-index='" + index + "']");
    // console.log($sequenceBoxes);
    
    if (feature.disabled()) {
      $sequenceBoxes.each(function(){
        if ($(this).attr("ori-fill") === undefined) {
          $(this).attr("ori-fill", $(this).css("fill"));
        }

        $(this).attr("disabled-sequence", "disabled-sequence")
               .css("fill", "#eee")
               .css("cursor", "auto");
      });
      $sequenceLabels.each(function(){
        if ($(this).attr("old-code") === undefined) {
          $(this).attr("old-code", $(this).html());
        }
        $(this).html("X").css("cursor", "auto");
      });

    } else {
      $sequenceBoxes.each(function(){
        $(this).removeAttr("disabled-sequence")
               .css("fill", $(this).attr("ori-fill"))
               .removeAttr("ori-fill")
               .css("cursor", "default");
      });
      $sequenceLabels.each(function(){
        $(this).html($(this).attr("old-code"))
               .removeAttr("old-code")
               .css("cursor", "default");
      });
    }
  });
}

FeatureManagerModule.disable = function(viewer){
  if (viewer.$el("btn_show_feature_manager").attr("disabled") == "disabled") return;
  viewer.$el("btn_hide_feature_manager").click();
  viewer.$el("btn_show_feature_manager").show().attr("disabled", "disabled");
  viewer.$el("btn_hide_feature_manager").attr("disabled", "disabled");
}

FeatureManagerModule.enable = function(viewer){
  if (viewer.$el("btn_show_feature_manager").attr("disabled") == undefined) return;
  viewer.$el("btn_hide_feature_manager").removeAttr("disabled")
  viewer.$el("btn_show_feature_manager").removeAttr("disabled").click();
}