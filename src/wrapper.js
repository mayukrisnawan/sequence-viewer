$.fn["singleSequenceViewer"] = function(options) {
  return new SingleSequenceViewer(this, options);
}

$.fn["multipleSequenceViewer"] = function(options) {
  return new MultipleSequenceViewer(this, options);
}