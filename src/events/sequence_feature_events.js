events["sequence_feature"] = {};

events["sequence_feature"].hoverFeature = function(viewer, $box) {
  if (multi(viewer) && viewer.alignmentMode()) {
    return false;
  }
  $box.css("cursor", "pointer");
  if ($box.$label) $box.$label.css("cursor", "pointer");

  var color = viewer.configs("hover_color");
  $box.css("fill", color);

  var feature = {
    name : $box.attr("data-feature-name"),
    type : $box.attr("data-feature-type"),
    from : Util.number($box.attr("data-feature-from")),
    to : Util.number($box.attr("data-feature-to"))
  };

  var msg = "#Site " + (feature.from+1) + "-" + (feature.to+1);
  msg += ", " + feature.name + " (" + feature.type + ")";
  viewer.notifyRight(msg);
}

events["sequence_feature"].unhoverFeature = function(viewer, $box) {
  if (multi(viewer) && viewer.alignmentMode()) {
    return false;
  }
  $box.css("cursor", "default");
  if ($box.$label) $box.$label.css("cursor", "default");

  var color = $box.attr("ori-color");
  $box.css("fill", color);
  viewer.notifyRight("");
}

events["sequence_feature"].box_hover = function(e){
  var viewer = e.data.viewer;
  var $box = $(this);
  events["sequence_feature"].hoverFeature(viewer, $box);
}

events["sequence_feature"].box_out = function(e){
  var viewer = e.data.viewer;
  var $box = $(this);
  events["sequence_feature"].unhoverFeature(viewer, $box);
}

events["sequence_feature"].label_hover = function(e){
  var viewer = e.data.viewer;
  var $label = $(this);
  var index = $label.attr("data-layer-index") + "_" + $label.attr("data-feature-index");
  if (multi(viewer)) {
    var row = $label.attr("data-row-index");
    var $box = viewer.$el("sequence_body").find(".svf_box_" + index + "[data-row-index='" + row + "']");
  } else {
    var $box = viewer.$el("sequence_body").find(".svf_box_" + index);
  }
  events["sequence_feature"].hoverFeature(viewer, $box);
}

events["sequence_feature"].label_out = function(e){
  var viewer = e.data.viewer;
  var $label = $(this);
  var index = $label.attr("data-layer-index") + "_" + $label.attr("data-feature-index");
  if (multi(viewer)) {
    var row = $label.attr("data-row-index");
    var $box = viewer.$el("sequence_body").find(".svf_box_" + index + "[data-row-index='" + row + "']");
  } else {
    var $box = viewer.$el("sequence_body").find(".svf_box_" + index);
  }
  events["sequence_feature"].unhoverFeature(viewer, $box);
}