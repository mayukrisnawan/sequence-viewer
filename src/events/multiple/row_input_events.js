events["row_input"] = {};
events["row_input"].update_row_input = function(e){
  var viewer = e.data.viewer,
      em = viewer.elementManager();
  var index = Util.number($(this).val()) - 1;

  if (index > viewer.sequences().count() - 1 || index == NaN || index < 0) index = 0;

  var $sequenceHeaders = em.$base().find(".msv_sequence_header");

  $sequenceHeaders.each(function(){
    var $this = $(this);
    var sequenceIndex = $this.attr("data-sequence-index");
    if (index == sequenceIndex) {
      $this.addClass("active");
    } else {
      $this.removeClass("active");
    }
  });
}