events["sequence_header.search_input"] = function(e){
  var viewer = e.data.viewer;
  var $input = viewer.$el("sequence_header.search_input"),
      query = $input.val();
  if (query == "") {
    viewer.notifyLeft("");
    return;
  }

  var sequences = viewer.sequences();
  var result = -1;
  for (var i =0; i < sequences.count(); i++) {
    var sequence = sequences.find(i);
    result = sequence.header().indexOf(query);
    if (result != -1) {
      result = i;
      break;
    }
  }
  if (result == -1) {
    viewer.notifyLeft("<i>Search not found for '" + query + "'</i>");
  } else {
    var $base = viewer.elementManager().$base();
    $base.find(".msv_sequence_header").removeClass("active");

    var $sequenceHeader = $base.find(".msv_sequence_header[data-sequence-index='" + result + "']");
    $sequenceHeader.addClass("active");

    var header = sequences.find(result).header();
    header = header.substr(0, 10) + "...";
    viewer.notifyLeft("<i>Search match with " + header + "</i>");

    viewer.notifyRow(result);
  }
}