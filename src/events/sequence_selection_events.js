events["toolbar.sequence_selection_mode"] = {};
events["toolbar.sequence_selection_mode"].change = function(e){
  var value = $(this).val();
  if (value == "single") {
    e.data.viewer.singleSelectionMode();
  } else {
    e.data.viewer.multipleSelectionMode();
  }
}

events["sequence_selection"] = {};

events["sequence_selection"].toggleSiteSelection = function(viewer, $box) {
  if ($box.attr("data-selected") === "true") {
  	viewer.unselectSite($box);
  } else {
  	viewer.selectSite($box);    
  }

  var output = [];
  var selectedSites = viewer.selectedSites();
  for (var i=0; i<selectedSites.length; i++) {
  	output.push(selectedSites[i].sequenceIndex);
  }
}

events["sequence_selection"].box_click = function(e){
  var viewer = e.data.viewer;
  if (multi(viewer) && viewer.alignmentMode()) return false;
  var $box = $(this);
  if ($box.attr("disabled-sequence")) return false;
  events["sequence_selection"].toggleSiteSelection(viewer, $box);
}

events["sequence_selection"].label_click = function(e){
  var viewer = e.data.viewer;
  if (multi(viewer) && viewer.alignmentMode()) return false;
  var $label = $(this);
  var index = $label.attr("data-sequence-index");
  if (multi(viewer)) {
    var row = $label.attr("data-row-index");
    var $box = viewer.$el("sequence_body").find(".sv_sequence_box_" + index + "[data-row-index='" + row + "']");
  } else {
    var $box = viewer.$el("sequence_body").find(".sv_sequence_box_" + index);
  }
  if ($box.attr("disabled-sequence")) return false;
  events["sequence_selection"].toggleSiteSelection(viewer, $box);
}