events["feature_manager"] = {};

events["feature_manager"].show = function(e){
  if ($(this).attr("disabled") == "disabled") return false;
  var viewer = e.data.viewer,
      $this = $(this),
      $fm = viewer.$el("feature_manager"),
      $btnHide = viewer.$el("btn_hide_feature_manager");
  $fm.slideDown(400, function(){
    $this.hide();
    $btnHide.show();
    SingleSVModules.setContainerDimension(viewer);
  });
}

events["feature_manager"].hide = function(e){
  var viewer = e.data.viewer,
      $this = $(this),
      $fm = viewer.$el("feature_manager"),
      $btnShow = viewer.$el("btn_show_feature_manager");
  $fm.slideUp(400, function(){
    $this.hide();
    $btnShow.show();
    SingleSVModules.setContainerDimension(viewer);
  });
}

events["feature_manager"].toggle_feature = function(e){
  var viewer = e.data.viewer;
  if (this.checked) {
    viewer.showFeatures();
  } else {
    viewer.hideFeatures();
  }
}