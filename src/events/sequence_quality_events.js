events["sequence_quality"] = {};

events["sequence_quality"].show = function(e){
  var viewer = e.data.viewer,
      $this = viewer.$el("btn_show_sequence_quality"),
      $sc = viewer.$el("sequence_quality"),
      $btnHide = viewer.$el("btn_hide_sequence_quality");
  $this.attr("disabled", "disabled");
  $sc.slideDown(400, function(){
    $this.hide();
    $btnHide.removeAttr("disabled").show();
    SingleSVModules.setContainerDimension(viewer);
  });
}

events["sequence_quality"].hide = function(e){
  var viewer = e.data.viewer,
      $this = viewer.$el("btn_hide_sequence_quality"),
      $sc = viewer.$el("sequence_quality"),
      $btnShow = viewer.$el("btn_show_sequence_quality");
  $this.attr("disabled", "disabled");
  $sc.slideUp(400, function(){
    $this.hide();
    $btnShow.removeAttr("disabled").show();
    SingleSVModules.setContainerDimension(viewer);
  });
}