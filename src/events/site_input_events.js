events["site_input"] = {};
events["site_input"].update_site_input = function(e){
  var viewer = e.data.viewer;
  
  if ($(this).val() == "") return;
  var index = Util.number($(this).val());
  if (index == NaN || index == 0) index = 1;
  index = index - 1;

  SingleSVModules.unhighlightSequence(viewer);
  
  var position ={
    index:index-2,
    start:index-2,
    finish:index-2,
    length:1
  };

  if (multi(viewer)) {
    viewer.sequences().each(function(sequence){
      sequence.position(position);
    });
  } else {
    viewer.sequence().position(position);
  }
  viewer.slideTo(index);
}