events["sequence_body"] = {};

events["sequence_body"].hoverSequence = function(viewer, $box) {
  if (multi(viewer) && viewer.alignmentMode()) {
    return false;
  }
  if ($box.attr("disabled-sequence")) return false;

  if ($box.attr("data-selected") !== "true") {
    var color = viewer.configs("hover_color");
    $box.css("fill", color)
    $box.attr("data-hovered", "true")
  }

  if ($box.attr("consensus-annotation") !== undefined) {
    var msg = $box.attr("consensus-annotation");
    viewer.notifyRight(msg);
    return;
  }

  var index = $box.attr("data-sequence-index");
  index = new Number(index).valueOf() + 1;
  var msg = "Site #" + index;
  if (multi(viewer)) {
    var row = $box.attr("data-row-index");
    msg += " " + MultipleSequenceBodyModule.sequenceDescription(viewer, row, index-1);
  } else {
    msg += " " + SequenceBodyModule.sequenceDescription(viewer, index-1);
  }
  viewer.notifyRight(msg);
}

events["sequence_body"].unhoverSequence = function(viewer, $box) {
  if (multi(viewer) && viewer.alignmentMode()) {
    return false;
  }

  if ($box.attr("disabled-sequence") || $box.attr("data-selected") === "true") return false;
  var color = $box.attr("ori-color"),
      highlighted = $box.attr("data-highlighted");
  if (highlighted == "true") {
    color = viewer.configs("highlight_color");
  }
  $box.css("fill", color);
  viewer.notifyRight("");
}

events["sequence_body"].box_hover = function(e){
  var viewer = e.data.viewer;
  var $box = $(this);
  events["sequence_body"].hoverSequence(viewer, $box);
}

events["sequence_body"].box_out = function(e){
  var viewer = e.data.viewer;
  var $box = $(this);
  events["sequence_body"].unhoverSequence(viewer, $box);
}

events["sequence_body"].label_hover = function(e){
  var viewer = e.data.viewer;
  var $label = $(this);
  var index = $label.attr("data-sequence-index");
  if (multi(viewer)) {
    var row = $label.attr("data-row-index");
    var $box = viewer.$el("sequence_body").find(".sv_sequence_box_" + index + "[data-row-index='" + row + "']");
  } else {
    var $box = viewer.$el("sequence_body").find(".sv_sequence_box_" + index);
  }
  $box.$label = $label;
  events["sequence_body"].hoverSequence(viewer, $box);
}

events["sequence_body"].label_out = function(e){
  var viewer = e.data.viewer;
  var $label = $(this);
  var index = $label.attr("data-sequence-index");
  if (multi(viewer)) {
    var row = $label.attr("data-row-index");
    var $box = viewer.$el("sequence_body").find(".sv_sequence_box_" + index + "[data-row-index='" + row + "']");
  } else {
    var $box = viewer.$el("sequence_body").find(".sv_sequence_box_" + index);
  }
  $box.$label = $label;
  events["sequence_body"].unhoverSequence(viewer, $box);
}

events["sequence_body"].consensus_label_hover = function(e){
  var viewer = e.data.viewer;
  var $label = $(this);
  var index = $label.attr("data-sequence-index");
  if (!multi(viewer)) return;
  var $box = viewer.elementManager().$base().find(".sv_consensus_box[data-sequence-index='" + index + "']");
  $box.$label = $label;
  events["sequence_body"].hoverSequence(viewer, $box);
}

events["sequence_body"].consensus_label_out = function(e){
var viewer = e.data.viewer;
  var $label = $(this);
  var index = $label.attr("data-sequence-index");
  if (!multi(viewer)) return;
  var $box = viewer.elementManager().$base().find(".sv_consensus_box[data-sequence-index='" + index + "']");
  $box.$label = $label;
  events["sequence_body"].unhoverSequence(viewer, $box);
}