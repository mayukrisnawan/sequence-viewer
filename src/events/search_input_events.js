events["search_input"] = {};
events["search_input"].search_sequence = function(e){
  if (e.keyCode == 13) {
    var pattern = $(this).val();
    pattern = pattern.toUpperCase();
    e.data.viewer.search(pattern);
  }
}

events["search_input"].flush_search_input = function(e){
  if ($(this).val() == "") {
    SingleSVModules.unhighlightSequence(e.data.viewer);
    e.data.viewer.notifyLeft("");
    return;
  }
}