events["zoom_slider"] = {
  moveBy:function(viewer, offset) {
    offset = Math.round(offset/4);
    var $slider = viewer.$el("toolbar.zoom_slider");
    if (!$slider.hasClass("clicked")) return false;

    var oldLeft = $slider.css("left").valueOf();
    oldLeft = oldLeft.replace("px", "");
    oldLeft = new Number(oldLeft).valueOf();

    var newLeft = (oldLeft + offset) % 101;
    if (newLeft < 0) newLeft = 0;
    $slider.css("left", newLeft);
  }
};
events["zoom_slider"].click = function(e){
  $(this).addClass("clicked"); 
}
events["zoom_slider"].mousemove = function(e){
  events["zoom_slider"].moveBy(e.data.viewer, e.offsetX);
}

events["zoom_line"] = {};
events["zoom_line"].mousemove = function(e){
  events["zoom_slider"].moveBy(e.data.viewer, e.offsetX);
}
