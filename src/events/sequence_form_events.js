events["toolbar.sequence_form"] = {};
events["toolbar.sequence_form"].change = function(e){
  var value = $(this).val();
  if (value == "protein") {
    e.data.viewer.setProteinMode(true);
  } else {
    e.data.viewer.setProteinMode(false);
  }
}