events["sequence_ambiguity"] = {};

events["sequence_ambiguity"].show = function(e){
  var viewer = e.data.viewer,
      $this = viewer.$el("btn_show_ambiguity_editor"),
      $sc = viewer.$el("ambiguity_editor"),
      $btnHide = viewer.$el("btn_hide_ambiguity_editor");
  $this.attr("disabled", "disabled");
  $sc.slideDown(400, function(){
    $this.hide();
    $btnHide.removeAttr("disabled").show();
    SingleSVModules.setContainerDimension(viewer);
  });
}

events["sequence_ambiguity"].hide = function(e){
  var viewer = e.data.viewer,
      $this = viewer.$el("btn_hide_ambiguity_editor"),
      $sc = viewer.$el("ambiguity_editor"),
      $btnShow = viewer.$el("btn_show_ambiguity_editor");
  $this.attr("disabled", "disabled");
  $sc.slideUp(400, function(){
    $this.hide();
    $btnShow.removeAttr("disabled").show();
    SingleSVModules.setContainerDimension(viewer);
  });
}