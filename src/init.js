var templates = [],
    events = [];

var sequenceColors = {
  "A":"#FFFFCF",
  "B":"#BCD56E",
  "C":"#CBE2FF",
  "D":"#A0EE6E",
  "E":"#E9C3AB",
  "F":"#BDE4BF",
  "G":"#E4C3F3",
  "H":"#C0D2E0",
  "I":"#CCD56E",
  "K":"#F1C1FA",
  "L":"#DAFCFF",
  "M":"#E9F1CA",
  "N":"#F7CBD3",
  "P":"#FAF8E7",
  "Q":"#FFDCC5",
  "R":"#D5F1A2",
  "S":"#C1E2E2",
  "T":"#FFB3C2",
  "U":"#D2F6FF",
  "V":"#F8C4CC",
  "W":"#B2B2F7",
  "Y":"#F89AC4",
  "Z":"#88D5EE",
  "*":"#555555",
  "?":"#555555"
}

var featureColors = [
  "#BCD56E",
  "#CBE2FF",
  "#A0EE6E",
  "#E9C3AB",
  "#BDE4BF",
  "#E4C3F3",
  "#C0D2E0",
  "#CCD56E",
  "#F1C1FA",
  "#DAFCFF",
  "#E9F1CA",
  "#F7CBD3",
  "#FAF8E7",
  "#ECEBDA",
  "#D5F1A2",
  "#C1E2E2",
  "#FFB3C2",
  "#D2F6FF",
  "#F8C4CC",
  "#B2B2F7",
  "#F89AC4",
  "#88D5EE",
  "#FFFFCF"
];

var nucleotideName = {
  "A" : "Adenin",
  "G" : "Guanin",
  "T" : "Thymine",
  "C" : "Cytosine",
  "U" : "Uracil",
  "-" : "gap"
};

var proteinName = {
  "A" : "(Ala/A) Alanine",
  "C" : "(Cys/C) Cysteine",
  "D" : "(Asp/D) Aspartic acid",
  "E" : "(Glu/E) Glutamic acid",
  "F" : "(Phe/F) Phenylalanine",
  "G" : "(Gly/G) Glycine",
  "H" : "(His/H) Histidine",
  "I" : "(Ile/I) Isoleucine",
  "K" : "(Lys/K) Lysine",
  "L" : "(Leu/L) Leucine",
  "M" : "(Met/M) Methionine",
  "N" : "(Asn/N) Asparagine",
  "P" : "(Pro/P) Proline",
  "Q" : "(Gln/Q) Glutamine",
  "R" : "(Arg/R) Arginine",
  "S" : "(Ser/S) Serine",
  "T" : "(Thr/T) Threonine",
  "V" : "(Val/V) Valine",
  "W" : "(Trp/W) Tryptophan",
  "Y" : "(Tyr/Y) Tyrosine",
  "TAA" : "Stop (Ochre)",
  "TAG" : "Stop (Amber)",
  "TGA" : "Stop (Opal)",
  "-" : "gap"
}

var multi = function(viewer) {
  return viewer.constructor === MultipleSequenceViewer;
}

var single = function(viewer) {
  return viewer.constructor === SingleSequenceViewer;
}

var deepCopy = function(source, destination) {
  for (var property in source) {
      if (typeof source[property] === "object" && source[property] !== null && destination[property]) { 
          deepCopy(destination[property], source[property]);
      } else {
          destination[property] = source[property];
      }
  }
};

