/*
 * Multiple Sequence Viewer Class
 * act as initializer for jQuery wrapper
 */
function MultipleSequenceViewer(signedTarget, signedOptions){
  var options = {};
  var $target = $(signedTarget);
  var viewer = this;
  var alignmentMode = false;
  var process = {};

  var constructOptions = function(){
    options = {
      fasta:{},
      // cut:{},
      alignment:{},
      //tree:{},
      tree:"",
      features:[],
      featureAdjustment:0
    }
    // Fasta Options
    if (!signedOptions["fasta"]) throw("Fasta file not available");
    if (signedOptions["fasta"].constructor == String) {
      options.fasta.url = signedOptions["fasta"];
    } else {
      deepCopy(signedOptions["fasta"], options.fasta);
    }

    // Cut Options
    // if (signedOptions["cut"] !== undefined) if (signedOptions["cut"].constructor == String) {
    //   options.cut.url = signedOptions["cut"];
    // } else {
    //   deepCopy(signedOptions["cut"], options.cut);
    // }

    // Feature Options
    if (signedOptions["features"] !== undefined) if (signedOptions["features"].constructor == String) {
      options.features = { url: signedOptions.features };
    } else {
      deepCopy(signedOptions.features, options.features);
    }

    // Alignment Options
    if (signedOptions["alignment"] !== undefined) if (signedOptions["alignment"].constructor == String) {
      options.alignment = { url: signedOptions.alignment };
    } else {
      deepCopy(signedOptions.alignment, options.alignment);
    }

    // Tree Options
    options.tree = signedOptions.tree;
    /*
    if (signedOptions["tree"] !== undefined) if (signedOptions["tree"].constructor == String) {
      options.tree = { url: signedOptions.tree };
    } else {
      deepCopy(signedOptions.tree, options.tree);
    }*/

    if (signedOptions.featureAdjustment) options.featureAdjustment = signedOptions.featureAdjustment;
  }
  constructOptions();

  var elements = new UI.ElementManager($target, {viewer:viewer});
  var sequences = null;
  var lastSearchIndex = {
    row:0,
    column:0
  };
  var featureManager = new FeatureManager();
  var templateManager = new UI.TemplateManager();

  var configs = {
    sequence_box_size : 20,
    feature_box_size : 14,
    highlight_color : "#2DABFF",
    hover_color : "#FFEE30"
  }

  /*
    Site Selection Functionality
  */
  var selectionMode = "multiple";
  var selectedSites = [];

  this.singleSelectionMode = function(){
    selectionMode = "single";
  }

  this.multipleSelectionMode = function(){
    selectionMode = "multiple";
  }

  this.selectionMode = function(){
    return selectionMode;
  }

  this.selectedSites = function(){
    return selectedSites;
  }

  this.clearSiteSelection = function(){
    selectedSites = [];
    this.$el("sequence_body").find(".sv_sequence_box[data-selected='true']").each(function(){
      $(this).click();
    });
    SequenceSelectionModule.notifySelectedSites(viewer);
  }

  this.selectSite = function($box){
    SequenceSelectionModule.selectSite(viewer, $box);
    SequenceSelectionModule.notifySelectedSites(viewer);
  }

  this.unselectSite = function($box){
    SequenceSelectionModule.unselectSite(viewer, $box);
    SequenceSelectionModule.notifySelectedSites(viewer);
  }

  this.deleteSelectedSites = function(){
    var result = confirm("Are you sure to delete selected sites from sequences?");
    if (result === true) {
      SequenceSelectionModule.deleteSelectedSites(viewer, options);
    }
  }
  /*
    /Site Selection Functionality
  */


  /*
   * Return Element Manager of the viewer
   * @return UI.ElementManager
   */
  this.elementManager = function(){
    return elements;
  }

  /*
   * Return Template Manager of the viewer
   * @return UI.TemplateManager
   */
  this.templateManager = function(){
    return templateManager;
  }

  /*
   * Return Feature Manager of the viewer
   * @return FeatureManager
   */
  this.featureManager = function(){
    return featureManager;
  }

  /*
   * Return sequence group object of the viewer
   * @return SequenceGroup
   */
  this.sequences = function(){
    return sequences;
  }

  /*
   * Get sequene viewer configuration
   * @args name String
   * @return Hash|Object
   */
  this.configs = function(name){
    if (name === undefined) return configs;
    return configs[name];
  }

  /*
   * Element selector helper
   * @return jQuerySelector
   */
  this.$el = function(name){
    var el = elements.get(name);
    if (el == false) throw "Element " + name + " not found";
    return el.$el();
  }

  /*
   * Element selector helper
   * @return jQuerySelector
   */
  this.dom = function(name){
    return elements.get(name).dom();
  }

  /*
   * Load sequences
   * @args options Hash
   */
  this.loadSequencesFrom = function(sequencesOptions, callback){
    var passedCallback = sequencesOptions.success;
    sequencesOptions.success = function(response){
      sequences = FastaFileParser.read(response, true);
      callback();
      if (passedCallback) passedCallback();
    }
    $.ajax(sequencesOptions);
  }

  /*
   * Load features
   * @args options Hash
   * @args callback Function
   */
  this.loadFeaturesFrom = function(featuresOptions, callback){
    // return immediately when feature options defined as an array
    if (featuresOptions.constructor == Array) {
      callback();
      return false;
    }

    var passedCallback = featuresOptions.success;
    featuresOptions.dataType = "json";
    featuresOptions.success = function(response){
      options.features = response;
      callback();
      if (passedCallback) passedCallback();
    }
    featuresOptions.complete = function(){
      // viewer.$el("btn_show_feature_manager").removeAttr("disabled");
      // viewer.$el("btn_hide_feature_manager").removeAttr("disabled");
      // viewer.notifyLeft("");
    }

    viewer.$el("btn_show_feature_manager").attr("disabled", "disabled");
    viewer.$el("btn_hide_feature_manager").attr("disabled", "disabled");
    viewer.notifyLeft("Loading sequence features..");
    $.ajax(featuresOptions);
  }

  /*
   * Load the ruler of the viewer
   */
  this.loadRuler = function(){
    var $container = this.$el("sequence_body");
    SequenceBodyModule.loadRuler(viewer, $container, sequences.max().sequence);
  }

  /*
   * Load sequence body
   */
  this.loadSequenceBody = function(callback){
    var $container = this.$el("sequence_body");
    MultipleSequenceBodyModule.loadSequenceBodySVG(viewer, $container, sequences, configs.sequence_box_size, callback);
    this.slideTo(0);
  }

  /*
   * Load sequence header
   */
  this.loadSequenceHeader = function(){
    this.$el("sequence_header.topbar").show();
    var $header = this.$el("sequence_header").html("");
    var oriWidth = $header.width();

    $header.parent().css("padding", 0);
    $header.parent().css("vertical-align", "top");
    $header.css("overflow-x", "scroll");

    sequences.each(function(sequence, index){
      var header = index == 0 ? "<div class='msv_sequence_header active' data-sequence-index='" + index + "'>" : "<div class='msv_sequence_header' data-sequence-index='" + index + "'>";
      header += (index+1) + ". "  + sequence.header() + "</div>";
      $header.append(header);

      var spacer = "<div class='msv_feature_spacer' data-sequence-index='" + index + "'></div>";
      $header.append(spacer);
    });
    var $headers = elements.$base().find(".msv_sequence_header");
    $headers.width($header.width());
    $header.css("max-width", oriWidth);

    $headers.click(function(){
      $headers.removeClass("active");
      var $this = $(this);
      $this.addClass("active");
      var row = $this.attr("data-sequence-index");
      viewer.notifyRow(row);
    });
  }

  /*
   * Load features to the viewer
   */
  this.loadFeatures = function(){
    MultipleSequenceFeatureModule.renderFeatureTo(this);
  }

  /*
   * Notify something in the right status bar
   * @args content String
   */ 
  this.notifyRight = function(content){
    this.$el("right_status").html(content);
  }

  /*
   * Notify something in the left status bar
   * @args content String
   */ 
  this.notifyLeft = function(content){
    this.$el("left_status").html(content);
  }

  /*
   * Flush notification of the viewer
   */
  this.flushNotification = function(){
    this.notifyLeft("");
    this.notifyRight("");
  }

  /*
   * Slide sequence body to specified index
   * @args sequenceIndex Number
   */
  this.slideTo = function(sequenceIndex){
    this.$el("site_input").html(sequenceIndex+1);
    this.$el("sequence_body").scrollLeft(sequenceIndex * configs.sequence_box_size);
    viewer.$el("site_input").val(sequenceIndex+1);
  }

  /*
   * Notify row index
   * @args index Number
   */ 
  this.notifyRow = function(index){
    $target.find(".msv_sequence_header").removeClass("active");
    var $sequenceHeader = $target.find(".msv_sequence_header[data-sequence-index='" + index + "']");
    $sequenceHeader.addClass("active");
    index = Util.number(index) + 1;
    viewer.$el("row_input").val(index);
  }

  /*
   * Set viewer to protein mode or nucleotide mode
   * @args protein Boolean
   */
  this.setProteinMode = function(protein) {
    this.flushProcess();
    if (protein) {
      sequences.proteinize();
    } else {
      sequences.unproteinize();
    }
    this.loadSequenceBody(function(){
      viewer.loadRuler();
      viewer.flushNotification();
      viewer.loadFeatures();
      SingleSVModules.setContainerDimension(viewer);
    });
  }

  this.inProteinMode = function() {
    return sequences.inProteinMode();
  }

  /*
   * Search with specified pattern
   * @args pattern String
   */
  this.search = function(pattern){
    var startingRow = Util.number(viewer.$el("row_input").val());
    startingRow -= 1;
    var result = sequences.search(pattern, startingRow);
    var message = "";
    
    if (result.index != -1) {
      SingleSVModules.highlightSequence(this, result.start, result.finish, result.row);
      this.notifyRow(result.row);
      message = "Found at " + (result.start + 1);
      if (result.start != result.finish) message += " to " + (result.finish + 1);
    } else {
      SingleSVModules.unhighlightSequence(this);
      this.slideTo(0);
      this.notifyRow(0);
      message = "<font color='red'>Search with query '" + pattern + "' has no result</font>";
    }
    this.notifyLeft("<i>" + message + "</i>");
  }


  /*
   * Checke the viewer is in alignment mode or not!
   */
  this.alignmentMode = function() {
    return alignmentMode;
  }

  /*
   * Align sequences
   */
  this.alignSequences = function(callback){
    this.flushProcess();
    var passedCallback = options.alignment.success;
    var $sb = viewer.$el("sequence_body");
    var $btn = viewer.$el("toolbar.alignment");
    this.flushNotification();

    options.alignment.success = function(response){
      sequences = FastaFileParser.read(response, true);
      MultipleSequenceBodyModule.normalState(viewer, $sb);
      viewer.loadSequenceHeader();
      viewer.loadSequenceBody(function(){
        viewer.loadRuler();
        viewer.loadFeatures();
        SingleSVModules.setContainerDimension(viewer);
        viewer.notifyRight("");
        if (callback !== undefined) callback();
        if (passedCallback) passedCallback();
        $btn.remove();

        // show feature manager
        viewer.$el("btn_show_feature_manager").click();
        // set mode to alignment mode
        alignmentMode = true;

        // reset sequence form input
        viewer.elementManager().$base().find(".sv_sequence_form").val("nucleotide");
      });
    }

    options.alignment.failed = function(){
      $btn.removeAttr("disabled");
    }

    // MultipleSequenceBodyModule.loadingState(viewer, $sb);
    viewer.notifyRight("Fetching from alignment source..");
    $btn.attr("disabled", "disabled");
    $.ajax(options.alignment);
  }

  this.attachProcess = function(name, fn){
    if (process[name] !== undefined) return false;
    process[name] = fn;
    // console.log(name + " attached");
  }

  this.processRunning = function(name){
    return process[name] === undefined ? false : true;
  }

  this.terminateProcess = function(name){
    if (process[name] == undefined) return false; 
    process[name] = undefined;
    // console.log(name + " terminated");
  }

  this.flushProcess = function(){
    process = {};
    // console.log("processes flushed");
  }

  /*
   * Show Phylogenetic Tree on new tab
   *
  this.showTree = function(){
    var $btn = viewer.$el("toolbar.tree");

    options.tree.success = function(response){
      console.log(response);
    }

    options.tree.failed = function(){
      $btn.removeAttr("disabled");
    }

    options.tree.complete = function(){
      $btn.removeAttr("disabled");
    }

    this.notifyLeft("Constructing phylogenetic tree..")
    $btn.attr("disabled", "disabled");
    $.ajax(options.tree);
  } */

  // this.showTree = function(){
  //   if (!options.tree == "") window.open(options.tree);
  // }

  /*
   * Initialize the sequence viewer
   */
  this.init = function(){
    MultipleSVModules.createContainer(viewer, $target);
    if (signedOptions["alignment"] === undefined) {
      $target.find(".msv_alignment").hide();
    }
    MultipleSVModules.registerElements(viewer);
    MultipleSVModules.registerEvents(viewer);
    viewer.loadSequenceHeader();
    SingleSVModules.setContainerDimension(viewer);
    viewer.loadSequenceBody(function(){
      viewer.loadRuler();
      if (options.features !== undefined) viewer.loadFeaturesFrom(options.features, function(){
        Util.iterate(options.features, function(feature){
          var f = new Feature(feature);
          f.adjustBy(options.featureAdjustment);
          featureManager.attach(f);
        });
        viewer.loadFeatures();
        FeatureManagerModule.loadFeatureList(viewer);
        SingleSVModules.setContainerDimension(viewer);
      });
    });
  }

  this.reload = function() {
    viewer.clearSiteSelection();
    viewer.notifyLeft("");
    viewer.notifyRight("Reloading viewer..");
    viewer.$el("sequence_body").html("");

    elements = new UI.ElementManager($target, {viewer:viewer});
    sequences = null;
    lastSearchIndex = {
      row:0,
      column:0
    };
    featureManager = new FeatureManager();
    templateManager = new UI.TemplateManager();
    alignmentMode = false;
    process = {};

    selectionMode = "multiple";

    constructOptions();
    // console.log(JSON.stringify(options.cut));
    $target.unbind("click");

    this.loadSequencesFrom(options.fasta, function(){  
      viewer.init();
    });
  }

  // initialize viewer immediatelly
  this.loadSequencesFrom(options.fasta, function(){  
    viewer.init();
  });
}