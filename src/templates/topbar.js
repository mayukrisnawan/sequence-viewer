templates["topbar"] = "\
<div class='sv_region sv_topbar'> \
  <div class='control-group'> \
    <div class='controls'> \
      <div class='input-append pull-right' style='margin:0px'> \
        <input type='text' class='sv_search_input' placeholder='type here, for example : AT/GG/C/ATT'/> \
        <span class='add-on'><i class='icon-search'></i></span> \
      </div> \
      <div class='input-prepend pull-left' style='margin: 0px 5px 0px 0px; display:none'> \
        <span class='add-on'>Current Row</span> \
        <input type='number' class='sv_row_input' value='1' min='1'/> \
      </div> \
      <div class='input-prepend pull-left' style='margin: 0px'> \
        <span class='add-on'>Go to</span> \
        <input type='number' class='sv_site_input' value='1' min='1'/> \
      </div> \
    </div> \
  </div> \
</div> \
";