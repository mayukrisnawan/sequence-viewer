templates["ambiguity_editor"] = "\
<div class='svae_container sv_toolbar_table'> \
  <div class='svae_header sv_toolbar_header'> \
    Ambiguity \
  </div> \
  <div class='svfm_body'> \
    <div> \
      <table class='table table-bordered svae_table'> \
        <thead class='sv_toolbar_table_header'> \
          <tr style='font-size : 10px;'> \
            <th width='2%'>#</th> \
            <th>Code</th> \
            <th>Position</th> \
            <th width='2%'>Replacement</th> \
          </tr> \
        </thead> \
        <tbody class='svae_list sv_toolbar_table_inner'> \
          <tr> \
            <td colspan='4'><center><i>There is no ambiguity in this sequence</i></center></td> \
          </tr> \
        </tbody> \
      </table> \
    </div> \
  </div> \
</div> \
";