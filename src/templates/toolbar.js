templates["toolbar"] = {};
templates["toolbar"].open = "\
<div class='sv_toolbar sv_region'> \
  <form class='form-inline' style='margin:0px'> \
    <select class='sv_sequence_form' style='width:150px;'> \
      <option value='nucleotide'>Nucleotide Form</option> \
      <option value='protein'>Protein Form</option> \
    </select> \
    <select class='sv_sequence_selection_mode' style='width:150px;'> \
      <option value='multiple'>Multiple Site Selection</option> \
      <option value='single'>Single Site Selection </option> \
    </select> \
    <a class='svfm_btn_show_fm btn btn-info btn-mini'><i class='icon-chevron-down icon-white'></i> Show Feature Manager</a> \
    <a class='svfm_btn_hide_fm btn btn-info btn-mini' style='display:none'><i class='icon-chevron-up icon-white'></i> Hide Feature Manager</a> \
    \
    <a class='btn btn-danger btn-mini svsc_btn_show_sc_table'> \
      <i class=' icon-chevron-down icon-white'></i> \
      Show Sequence Quality \
      <span class='svsc_errors_count'></span> \
    </a> \
    \
    \
    <a class='btn btn-danger btn-mini svsc_btn_hide_sc_table' style='display:none'> \
      <i class='icon-chevron-up icon-white'></i> \
      Hide Sequence Quality \
      <span class='svsc_errors_count'></span> \
    </a> \
    \
    \
    <a class='btn btn-warning btn-mini msv_alignment'> \
      Align sequences \
    </a> \
    \
    \
    \
    \
    <a class='btn btn-success btn-mini svae_btn_show'> \
      <i class=' icon-chevron-down icon-white'></i> \
      Show Ambiguity Editor \
      <span class='svae_count'></span> \
    </a> \
    \
    \
    <a class='btn btn-success btn-mini svae_btn_hide' style='display:none'> \
      <i class=' icon-chevron-up icon-white'></i> \
      Hide Ambiguity Editor \
      <span class='svae_count'></span> \
    </a> \
    \
    <div class='pull-right' style='display:none'> \
      <span>Zoom</span> \
      - \
        <span class='sv_zoom_line'> \
          <span class='sv_zoom_slider'></span> \
        </span> \
      + \
    </div> \
  </form> \
";

/*
    <a class='btn btn-success btn-mini msv_tree'> \
      Phylogenetic Tree \
    </a> \
*/

templates["toolbar"].close = "</div>";