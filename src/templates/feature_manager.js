templates["feature_manager"] = "\
<div class='svfm_container sv_toolbar_table'> \
  <div class='svfm_header sv_toolbar_header'> \
    Feature Manager \
  </div> \
  <div class='svfm_body'> \
    <form class='form-inline' style='display:none'> \
      <input type='checkbox' class='svfm_toggle_features'/> \
      Show Feature \
    </form> \
    <div> \
      <table class='table table-bordered svfm_feature_table'> \
        <thead class='sv_toolbar_table_header'> \
          <tr style='font-size : 10px;'> \
            <th width='2%' style='margin:0; padding:0px 8px 0px 8px'><input type='checkbox' class='svfm_feature_checkbox_all' disabled checked/></th> \
            <th width='2%'>#</th> \
            <th>Feature Name</th> \
            <th>Length</th> \
            <th>Start Position</th> \
            <th>Finish Position</th> \
            <th>Start Motif</th> \
            <th>Finish Motif</th> \
          </tr> \
        </thead> \
        <tbody class='svfm_feature_list sv_toolbar_table_inner '> \
          <tr> \
            <td colspan='8'><center><i>No features added</i></center></td> \
          </tr> \
        </tbody> \
      </table> \
    </div> \
  </div> \
</div> \
";