templates["midbar"] = "\
<div class='sv_midbar'> \
  <table style='width:100%'> \
    <tr class='msv_consensus_bar'> \
      <td class='msv_consensus_label' width='26.7%' style='padding-top:2px; background-color : #eee; text-align: right; padding-right: 5px; border-right: solid 1px #eee'> \
        <b>CONSENSUS</b> \
      </td> \
      <td style='padding:0px; vertical-align:top'> \
        <div class='msv_consensus'> \
          <span style='padding: 2px 5px 0px 5px'></span> \
        </div> \
      </td> \
    </tr> \
\
    <tr class='sv_protein_preview_bar'> \
      <td class='sv_protein_preview_label' width='26.7%' style='padding-top:2px; background-color : #eee; text-align: right; padding-right: 5px; border-right: solid 1px #eee'> \
        <b>PROTEIN FORM</b> \
      </td> \
      <td style='padding:0px; vertical-align:top'> \
        <div class='sv_protein_preview'> \
          <span style='padding: 2px 5px 0px 5px'></span> \
        </div> \
      </td> \
    </tr> \
\
    <tr> \
      <td class='sv_sequence_header_outer' width='25%'> \
        <div class='msv_sequence_header_topbar'> \
          <div class='input-append pull-right' style='margin:0px'> \
            <input type='text' class='msv_search_header_input' placeholder='search sequence by header..' style=''/> \
            <span class='add-on'><i class='icon-search'></i></span> \
          </div> \
        </div> \
        <div class='sv_sequence_header'> \
          <center><i>Empty sequence header</i></center> \
        </div> \
      </td> \
      <td style='padding:0px; vertical-align:top'> \
        <div class='sv_sequence_body'> \
          <center style='height:40px; padding:20px 0px 0px 0px;'><i>Empty sequence body</i></center> \
        </div> \
      </td> \
    </tr> \
  </table> \
</div> \
";