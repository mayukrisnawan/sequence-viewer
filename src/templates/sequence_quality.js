templates["sequence_quality"] = "\
<div class='svsc_container sv_toolbar_table'> \
  <div class='svsc_header sv_toolbar_header'> \
    Sequence Quality \
  </div> \
  <div class='svfm_body'> \
    <div> \
      <table class='table table-bordered svsc_table'> \
        <tbody class='svsc_list sv_toolbar_table_inner'> \
          <tr> \
            <td colspan='2'><center><i>No quality problems found</i></center></td> \
          </tr> \
        </tbody> \
      </table> \
    </div> \
  </div> \
</div> \
";