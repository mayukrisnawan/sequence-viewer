UI.Element = function(signedName, selectorExpression, $signedBase){
  var name = signedName,
      $base = $signedBase === undefined ? $(document.body) : $signedBase,
      $el = $base.find(selectorExpression);

  /*
   * Return name of current element
   * @return String
   */
  this.name = function(){
    return name;
  }

  /*
   * Return element as jQuerySelector Object
   * @return jQuerySelector
   */
  this.$el = function(){
    return $el;
  }

  /*
   * Return element as Document Object Model Node
   * @return DOMElement
   */
  this.dom = function(){
    if ($el == null) return null;
    return $el.get(0);
  }

  /*
   * Bind event handler to the element
   * @args type String
   * @args action Function
   */
  this.event = function(type, action, data){
    $el.unbind(type).on(type, data, action);
  }

  /*
   * Bind late event handler to the element
   * Late Event is an event handled by element parent as a element base
   * @args type String
   * @args action Function
   */
  this.lateEvent = function(type, action, data){
    $base.on(type, selectorExpression, data, action);
  }
}