UI.ElementManager = function(baseElement, assignedEventDataBAse){
  var elements = [],
      $base = $(baseElement),
      eventDataBase = assignedEventDataBAse;

  function createEventDataBy(data) {
    var result = {};
    for (var key in eventDataBase) result[key] = eventDataBase[key];
    for (var key in data) result[key] = data[key];
    return result;
  }

  /*
   * Return base element of element manager
   * @return jQuerySelector
   */
  this.$base = function(){
    return $base;
  }

  /*
   * Return user interface element by name
   * @args name String
   * @return UI.Element
   */
  this.get = function(name){
    var result = false;
    Util.iterate(elements, function(element){
      if (element.name() == name) {
        result = element;
        return Util.stopIterate();
      }
    });
    return result;
  }

  /*
   * Return all elements of Element Manager
   * @return array of UI.Element
   */
  this.all = function(){
    return elements;
  }

  /*
   * Add new element to the User Interface Manager
   * @args name String
   * @args selectorExpression String
   */
  this.attach = function(name, selectorExpression){
    var newElement = new UI.Element(name, selectorExpression, $base);
    // console.log(newElement.name())
    // console.log(newElement.dom())
    if (elements.length != 0) {
      for (var i=0; i<elements.length; i++) {
        var element = elements[i];
        if (element.name() == name) {
          elements.splice(i, 1, newElement);
          return;
        }
      }
    }
    elements.push(newElement);
  }

  /*
   * Bind an event handler to the specified element
   * @args name String
   * @args type String
   * @args action Function
   */
  this.event = function(name, type, action, data){
    var element = this.get(name);
    var passedData = createEventDataBy(data);
    element.event(type, action, passedData);
  }

  /*
   * Bind late event handler to the element
   * Late Event is an event handled by element parent as a element base
   * @args name String
   * @args type String
   * @args action Function
   */
  this.lateEvent = function(name, type, action, data){
    var element = this.get(name);
    var passedData = createEventDataBy(data);
    element.lateEvent(type, action, passedData);
  }
}