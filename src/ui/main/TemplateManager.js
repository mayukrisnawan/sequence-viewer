/*
 * TemplateManager Class
 * Class for managing template
 */
UI.TemplateManager = function(){
  var templates = [];

  /*
   * flush template contents
   */
  this.flush = function(){
    templates = [];
  }

  /*
   * prepend template contents
   * @args contents String
   */
  this.prepend = function(contents){
    templates.unshift(contents);
  }

  /*
   * append template contents
   * @args contents String
   */
  this.append = function(contents){
    templates.push(contents);
  }

  /*
   * return contents of the template
   * @return String
   */
  this.contents = function(){
    return templates.join("\n");
  }   
}