/*
 * Fasta File Parser
 */
function FastaFileParser(){}

/*
 * Read from fasta file content
 * @return Sequence, array of Sequence
 * @args content String, multiple Boolean
 */
FastaFileParser.read = function(content, multiple){
  if (multiple === undefined) multiple = false;
  content = content.trim("\n");
  var lines = content.split("\n");

  var sequence = new Sequence();
  var sequences = new SequenceGroup();
  var sequenceCount = 0;

  for (var i = 0; i<lines.length; i++) {
    var line = lines[i].trim("\n");
    if (line[0] == ">") {
      line = line.substr(1, line.length)
      // stop parsing when single parsing condition found
      if (!multiple && sequenceCount > 0) {
        break;
      }

      // attach sequence header
      if (multiple) {
        var tmp = new Sequence();
        sequences.attach(tmp);
        sequences.find(sequenceCount).header(line);
      } else {
        sequence.header(line);
      }
      sequenceCount++;
    } else {

      // append sequence body
      if (multiple) {
        sequences.find(sequenceCount-1).appendBody(line, function(sequence){
          sequences.calculateMaxLengthBy(sequence);
        });
      } else {
        sequence.appendBody(line);
      }
    }
  }

  // return the parsed file
  if (multiple) {
    return sequences;
  } else {
    return sequence;
  }
}