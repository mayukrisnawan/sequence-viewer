function Range(_start, _finish) {
  // Initialize default properties
  var start = 0,
      finish = 0;
  if (_start) start = _start;
  if (_finish) finish = _finish;

  // Revert start and finish
  // when position not appropriate
  if (start > finish) {
    var tmp = start;
    start = finish;
    finish = tmp;
  }

  /*
   * return Range position
   * @return Hash
   */
  this.hashify = function(){
    return {
      start: start,
      finish: finish
    }
  }

  this.start = function() { return start; }
  this.finish = function() { return finish; }

  /*
   * Adjust range offset by index
   * @args adjustmentIndex Number
   */
  this.adjustBy = function(adjustmentIndex) {
    start += adjustmentIndex;
    finish += adjustmentIndex;
    return this;
  }

  /*
   * Check that range intersect with other Range object
   * @args range Range
   * @return Boolean
   */
  this.intersect = function(range){
    var other = range.hashify();
    if (other.start > finish) return false;
    if (start > other.finish) return false;
    return true;
  }
}