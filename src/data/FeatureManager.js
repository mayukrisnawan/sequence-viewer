function FeatureManager(){
  var features = [],
      matched = [];

  /*
   * Find speciied feature by specified index
   * @args index Number
   * @return Feature
   */
  this.find = function(index){
    return features[index];
  }

  /*
   * Find all features
   * @return array of Feature
   */
  this.all = function(){
    return features;
  }

  /*
   * Add new feature
   * @args feature Feature
   */
  this.attach = function(feature){
    features.push(feature);
    layers = null;
  }

  /*
   * Match feature with array of sequence
   * @args sequences array of Sequence
   * @return array of Hash
   */
  var matchWith = function(sequences) {
    matched = [];
    Util.iterate(sequences, function(sequence, index){
      matched[index] = [];
      Util.iterate(features, function(feature, featureIndex){
        matched[index].push({
          feature:feature,
          result:false
        })
        if (feature.match(sequence)) {
          matched[index][featureIndex].result = feature.matched();
        }
        // console.log(index + "," + featureIndex);
        // console.log(sequence.body());
        // console.log(feature.motifs());
        // console.log(feature.ranges()[1].hashify());
        // console.log("");
      })
    })
    return matched;
  }

  /*
   * Match feature with sequence or sequence group
   * @args input Sequence|SequenceGroup
   * @return array of Hash
   */
  this.match = function(input){
    if (input.constructor == Sequence) {
      return matchWith([input]);
    } else if (input.constructor == SequenceGroup) {
      return matchWith(input.all());
    }
  }

  /*
   * Match feature with sequence or sequence group
   * @args input Sequence|SequenceGroup
   * @return array of Hash
   */
  this.layers = function(sequence){
    var results = this.match(sequence);
    var inputs = results[0];

    var layers = [];
    Util.iterate(inputs, function(input, index){
      if (input.result == false) return;
      if (input.feature.disabled()) return;
      var feature = {
        index: index,
        name: input.feature.name(),
        type: input.feature.type(),
        range: new Range(input.result.start.index, input.result.finish.index),
        from: input.result.start.index,
        to: input.result.finish.index
      };
      
      var targetLayerIndex = 0;
      for (var layerIndex=0; layerIndex<layers.length; layerIndex++) {
        var currentLayer = layers[layerIndex];
        for (var featureIndex = 0; featureIndex < currentLayer.length; featureIndex++) {
          var currentFeature = currentLayer[featureIndex];
          if (currentFeature.range.intersect(feature.range)) targetLayerIndex++;
        }
      }

      // push feature to the feature layers
      if (layers[targetLayerIndex] === undefined) layers[targetLayerIndex] = [];
      layers[targetLayerIndex].push(feature);
    });
    return layers;
  }

  /*
   * return matched information
   * @return array of Hash
   */
  this.matched = function() {
    return matched;
  }

  /*
   * return matched information by specified index
   * @args index Number
   * @return Hash
   */
  this.findMatched = function(index){
    return matched[index];
  }

  /* OLD
   * Validate sequence with specified features
   * @args sequence Sequence
   * @return Boolean
  this.validate = function(sequence){
    var results = [];
    Util.iterate(features, function(feature, featureIndex){
      if (!feature.isForValidation()) return;
      if (!feature.match(sequence)) {
        results.push({
          index : featureIndex,
          name : feature.name()
        });
      }
    });
    return results;
  }
  */

  this.validate = function(sequence){
    var results = [];
    var sequenceBody = sequence.body();
    Util.iterate(features, function(feature){
      if (!feature.isForValidation()) return;
      var result = [];
      if (SequenceValidation.exactlyMatch(sequenceBody, feature)) return;

      var is_too_short = SequenceValidation.tooShort(sequenceBody, feature);
      var is_too_long = SequenceValidation.tooLong(sequenceBody, feature);

      if (is_too_short) {
        result.push({
          message : feature.name() + " has been found in the sequence, but the length is too short"
        })
      } else if (is_too_long) {
        result.push({
          message : feature.name() + " has been found in the sequence, but the length is too long"
        })
      }

      var is_novel = SequenceValidation.novel(sequenceBody, feature);
      if (is_novel && !is_too_long && !is_too_short) {
        var message = feature.name();
        if (SequenceValidation.correctStartMotifPosition(sequenceBody, feature)) {
          message += " has correct start motif (" + feature.motifs().start + ") position, but finish motif is missing";
        } else if (SequenceValidation.correctFinishMotifPosition(sequenceBody, feature)) {
          message += " has correct finish motif (" + feature.motifs().finish + ") position, but start motif is missing";
        }
        message += ". so, it has a novel position";
        result.push({
          message : message
        });
      }

      var is_partially_missing = SequenceValidation.partiallyMissing(sequenceBody, feature);
      if (is_partially_missing && !is_novel) result.push({
        message : feature.name() + " is partially missing"
      });

      if (!is_partially_missing && !is_too_long && !is_too_short && !is_novel) result.push({
        message : feature.name() + " is missing"
      });


      results = Util.merge(results, result);
    });
    return results;
  }

  this.disabledCount = function(){
    var count = 0;
    Util.iterate(features, function(feature){
      if (feature.disabled()) count++;
    });
    return count;
  }
}