/*
 * DNA Sequence object
 * @args properties Object
 */
function Sequence(properties){
  // Initialize default properties
  if (properties) {
    var header = properties.header ? properties.header :  "";
    var body = properties.body ? properties.body :  "";
  } else {
    var header = "";
    var body = "";
  }

  // Private members
  var lastPosition = {
    index:-1,
    start:-1,
    finish:-1,
    length:-1
  };
  var proteinMode = false;
  var proteinBody = "";
  var proteinTable = {
    "A":["GCT", "GCC", "GCA", "GCG",
       "GCU"],
    "C":["TGC", "TGT",
       "UGC", "UGU"],
    "D":["GAC", "GAT", 
        "GAU"],
    "E":["GAA", "GAG"],
    "F":["TTT", "TTC", 
       "UUU", "UUC"],
    "G":["GGA", "GGC", "GGG", "GGT"],
    "H":["CAT", "CAC",
       "CAU"],
    "I":["ATT", "ATC", "ATA",
       "AUU", "AUC", "AUA"],
    "K":["AAA", "AAG"],
    "L":["CTT", "CTC", "CTA", "CTG", "TTA", "TTG",
       "CUU", "CUC", "CUA", "CUG", "UUA", "UUG"],
    "M":["ATG",
       "AUG"],
    "N":["AAC", "AAT",
       "AAU"],
    "P":["CCT", "CCC", "CCA", "CCG",
       "CCU"],
    "Q":["CAA", "CAG"],
    "R":["AGA", "AGG", "CGA", "CGC", "CGG", "CGT",
       "CGU"],
    "S":["AGC", "AGT", "TCA", "TCC", "TCG", "TCT",
       "AGU", "UCA", "UCC", "UCG", "UCU"],
    "T":["ACT", "ACC", "ACA", "ACG",
       "ACU"],
    "V":["GTT", "GTC", "GTA", "GTG",
       "GUU", "GUC", "GUA", "GUG"],
    "W":["TGG",
       "UGG"],
    "Y":["TAT", "TAC",
       "UAU", "UAC"],
    "Z":["CAA", "CAG", "GAA", "GAG"],
    "*":["TAA", "TAG", "TGA",
       "UAA", "UAG", "UGA"],
  }

  // Public members
  this.rawFasta = function(){
    return ">" + header + "\n" + body;
  }

  /*
   * set or get sequence header
   */
  this.header = function(newHeader, callback) {
    if (!newHeader) return header;
    header = newHeader;
    if (callback != undefined) callback(this);
  }

  /*
   * set or get sequence body
   */
  this.body = function(newBody, callback) {
    if (!newBody) return proteinMode ? proteinBody : body;
    body = newBody.trim("\n");
    if (proteinMode) proteinBody = this.protein();
    if (callback != undefined) callback(this);
  }

  this.edit = function(index, newCode) {
    body = body.substr(0, index) + newCode + body.substr(index+1);
    if (proteinMode) proteinBody = this.protein();
  }

  /*
   * append sequence body
   */
  this.appendBody = function(adder, callback) {
    body += adder.trim("\n");
    if (callback != undefined) callback(this);
  }

  /*
   * Search with specified pattern
   * @args pattern String
   * @return Object
   */
  this.search = function(pattern, startingIndex){
    if (startingIndex !== undefined) lastPosition.index = startingIndex;
    var regex = PatternEngine.buildRegex(pattern);
    if (lastPosition.index == -1) {
      regex.lastIndex = null;
    } else {
      regex.lastIndex = lastPosition.index + 1;
    }

    var searchSequence = function(){
      try {
        var result = proteinMode ? regex.exec(proteinBody) : regex.exec(body);
        lastIndex = regex.lastIndex;
      } catch(e) {
        lastIndex = 0;
        return false;
      }

      var searchResult = result == null ? {
        index:-1,
        length:-1,
        start:-1,
        finish:-1
      } : {
        index:result.index,
        length:result[0].length,
        start:result.index,
        finish:result.index + result[0].length-1
      }
      lastPosition = searchResult;
      return searchResult;
    }

    var result = searchSequence();

    // if not found, try to search from beginning
    if (result.index == -1) {
      lastIndex = 0;
      regex.lastIndex = null;
      result = searchSequence();
      result.last = true;
    } else {
      // Indicating the search result not at near the end of sequence
      result.last = false;
    }
    return result;
  }

  /*
   * Reset search to first position
   */
  this.resetSearch = function() {
    lastPosition = {
      index:-1,
      start:-1,
      finish:-1,
      length:-1
    };
  }

  /*
   * return last position
   * @return Number
   */
  this.position = function(newPosition){
    if (newPosition !== undefined) lastPosition = newPosition;
    var result = new Object(lastPosition);
    if (result.index == -1) result.index = 0;
    if (result.length == -1) result.length = 0;
    if (result.start == -1) result.start = 0;
    if (result.finish == -1) result.finish = 0;
    return lastPosition;
  }

  /*
   * return protein form of sequence body
   * @return String
   */
  this.protein = function(){
    return Sequence.proteinize(body, proteinTable);
  }

  /*
   * return nucleotide form of sequence body
   * @return String
   */
  this.nucleotide = function(){
    return body;
  }

  /*
   * turn sequence to protein mode
   * @return String
   */
  this.proteinize = function(){
    proteinMode = true;
    proteinBody = this.protein();
    return proteinBody;
  }

  /*
   * turn sequence to nucleotide mode
   * @return String
   */
  this.unproteinize = function(){
    proteinMode = false;
    return body;
  }

  /*
   * return the indicator of protein mode
   * @return Boolean
   */
  this.inProteinMode = function(){
    return proteinMode;
  }

  this.remove = function(selectedSites, callback) {
    for (var i = 0; i<selectedSites.length; i++) {
      var selectedSite = selectedSites[i];
      var index = selectedSite.sequenceIndex - i;
      if (proteinMode) {
        index = index * 3;
        body = body.substr(0, index) + body.substr(index+3);
      } else {
        body = body.substr(0, index) + body.substr(index+1);
      }
    }
    if (proteinMode) this.proteinize();
    if (callback) callback();
  }

  this.computeProteinPreview = function(){
    var proteins = [];

    var s1 = null,
        s2 = null,
        s3 = null;

    if (body.length == 0) return proteins;
    if (body.length >= 3) s1 = body.substring(0, body.length);
    if (body.length >= 4) s2 = body.substring(1, body.length);
    if (body.length >= 5) s3 = body.substring(2, body.length);

    if (s1 != null) proteins.push(Sequence.proteinize(s1, proteinTable));
    if (s2 != null) proteins.push(Sequence.proteinize(s2, proteinTable));
    if (s3 != null) proteins.push(Sequence.proteinize(s3, proteinTable));

    return proteins;
  }
}

Sequence.proteinize = function(source, proteinTable){
  var result = "";
  var translate = function(codon){
    if (codon == "---") return "-";
    for (var proteinForm in proteinTable) {
      var nucleotideForms = proteinTable[proteinForm];
      for (var i=0; i<nucleotideForms.length; i++) {
        var nucleotide = nucleotideForms[i];
        if (nucleotide == codon) return proteinForm;
      }
    }
    return "?";
  }
  for (var i=0; i<source.length; i=i+3) {
    var codon = source.substring(i, i+3);
    if (codon.length != 3) continue;
    result += translate(codon);
  }
  return result;
}