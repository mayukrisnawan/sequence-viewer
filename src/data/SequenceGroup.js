/*
 * SequenceGroup
 * @desc container for multiple sequence
 */
function SequenceGroup(){
  var sequences = [];
  var maxLength = 0,
      longestSequence = null;
  var currentRow = 0;
  var proteinMode = false;
  var consensus = null;

  /*
   * find sequence by index
   * @args index Number
   * @return Sequence
   */
  this.find = function(index){
    return sequences[index];
  }

  /*
   * all sequences
   * @return array of Sequence
   */
  this.all = function(){
    return sequences;
  }

  /*
   * return sequences count
   * @return Number
   */
  this.count = function(){
    return sequences.length;
  }

  /*
   * max length of the sequences
   * @return Number
   */
  this.max = function(){
    return {
      length: maxLength,
      sequence: longestSequence
    }
  }

  /*
   * attach Sequence at the end
   * @args sequence Sequence
   */
  this.attach = function(sequence){
    if (sequence.constructor == Sequence) {
      if (longestSequence === null) longestSequence = sequence;
      sequences.push(sequence);
      consensus = null;
    }
  }

  /*
   * each iterator
   * @args action Function
   */
  this.each = function(action){
    for (var i=0; i<sequences.length; i++) {
      action(sequences[i], i);
    }
  }

  /*
   * Calculate longest sequence by including new sequence
   */
  this.calculateMaxLengthBy = function(sequence) {
    var newLength = sequence.body().length;
    if (newLength > maxLength) {
      maxLength = newLength;
      longestSequence = sequence;
    }
  }

  /*
   * Search with specified pattern
   * @args pattern String
   * @args startingRow Number
   * @return Object
   */
  this.search = function(pattern, startingRow) {
    var sequence, result = {
      index:-1,
      start:-1,
      finish:-1,
      length:-1
    };
    if (startingRow !== undefined) currentRow = startingRow;

    if (currentRow >= sequences.length) currentRow = 0;

    var searchFromTo = function(from, to){
      for (var i=from; i<=to; i++) {
        sequence = sequences[i];
        do {
          result = sequence.search(pattern);
        } while (result.index == -1 && !result.last);
        if (result.index != -1) {
          if (result.last) {
            if (i == sequences.length-1) {
              result.index = -1;
              break;
            }
          } else {
            currentRow = i;
            break;
          }
        }
      }
    }
    searchFromTo(currentRow, sequences.length-1);

    if (result.index == -1) {
      // almost at the end of the row, so return to the first row
      if (currentRow == sequences.length-1 && sequences.length != 1) {
        currentRow = 0;
        searchFromTo(currentRow, sequences.length-2);
      }
    }
    result.row = currentRow;
    return result;
  }

  /*
   * get current position of active sequence
   * @return Object
   */
  this.position = function(){
    //console.log(currentRow);
    var sequence = sequences[currentRow];
    var _position = sequence.position();
    var position = {};
    for (var property in _position) {
      if (property != "last") position[property] = _position[property];
    }
    position.row = currentRow;
    return position;
  }

  /*
   * Sort sequences by header
   */
  this.sort = function(){
    for (var i=0; i<sequences.length-1; i++) {
      for (var j=i+1; j<sequences.length; j++) {
        var first = sequences[i];
        var second = sequences[j];
        if (first.header() > second.header()) {
          sequences[i] = second;
          sequences[j] = first;
        }
      }
    }
  }

  /*
   * turn sequence to protein mode
   * @return String
   */
  this.proteinize = function(){
    proteinMode = true;
    consensus = null;
    for (var i=0; i<sequences.length; i++) {
      sequences[i].proteinize();
    }
    return this.all().map(function(sequence){
      return sequence.body();
    });
  }

  /*
   * turn sequence to nucleotide mode
   * @return String
   */
  this.unproteinize = function(){
    proteinMode = false;
    consensus = null;
    for (var i=0; i<sequences.length; i++) {
      sequences[i].unproteinize();
    }
    return this.all().map(function(sequence){
      return sequence.body();
    });
  }

  /*
   * return the indicator of protein mode
   * @return Boolean
   */
  this.inProteinMode = function(){
    return proteinMode;
  }

  this.remove = function(selectedSites, callback) {
    consensus = null;
    var sites = [];
    for (var i = 0; i < selectedSites.length; i++) {
      var selectedSite = selectedSites[i];
      if (sites[selectedSite.rowIndex] === undefined) sites[selectedSite.rowIndex] = [];
      sites[selectedSite.rowIndex].push(selectedSite);
    }
    this.each(function(sequence, index){
      sequence.remove(sites[index]);
    });
    if (callback) callback();
  }

  this.consensus = function(){
    if (sequences.length == 1) return sequences[0].body();
    if (consensus !== null) return consensus;
    count_list = [];

    // collect count_list of genetic code from all sequences
    for (var i=0; i<sequences.length; i++) {
      var sequence = sequences[i];
      for (var j=0; j<maxLength; j++) {
        if (j >= sequence.body().length) break;
        var code = sequence.body()[j];
        if (count_list[j] === undefined) count_list[j] = {};
        if (count_list[j][code] === undefined) {
          count_list[j][code] = 1;
        } else {
          count_list[j][code] += 1;
        }
      }
    }

    consensus = "";

    for (var i=0; i<count_list.length; i++) {
      var count = count_list[i];
      var result = "-";
      var max = null;
      var same;
      for (var code in count) {
        if (max === null || count[code] > max) {
          max = { code: code, count: count[code] }
          same = false;
          result = code;
        } else if (count[code] == max.count) {
          same = true;
        }
      }
      if (same === true) result = "-";
      consensus += result;
    }

    return consensus;
  }
}