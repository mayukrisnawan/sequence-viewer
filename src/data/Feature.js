/* 
 * Feature Class
 * Holding Information about some parts of sequence
 */
function Feature(properties){
  //
  // Initialize default properties
  //
  var name = "",
      type = "",
      rangeExpression = {start:"", finish:""},
      ranges = [],
      motifs = {start:"", finish:""},
      lengthExpression = "",
      lengths = [],
      isForValidation = false;

  var matched = {
    start:{
      index:-1,
      from:-1,
      to:-1,
      motif:""
    },
    finish:{
      index:-1,
      from:-1,
      to:-1,
      motif:""
    },
    range:new Range(-1,-1),
    length:-1
  };
      
  var disabled = false;

  //
  // Initialize property by passed configurations
  //
  if (properties) {
    if (properties.name) name = properties.name;
    if (properties.type) type = properties.type;

    if (properties.range_expression) rangeExpression = properties.range_expression;
    if (properties.rangeExpression) rangeExpression = properties.rangeExpression;
    if (properties.ranges) ranges = properties.ranges;

    if (properties.length_expression) lengthExpression = properties.length_expression;
    if (properties.lengthExpression) lengthExpression = properties.lengthExpression;

    if (properties.motifs) motifs = properties.motifs;
    if (properties.isForValidation) isForValidation = properties.isForValidation;
  }

  //
  // Generate ranges from range expression
  //
  var generateRanges = function() {
    if (rangeExpression.start.length != 0 && rangeExpression.finish.length != 0) {
      ranges = PatternEngine.generateRanges({
        start : rangeExpression.start,
        finish : rangeExpression.finish
      });
    }
  }
  //
  // Generate ranges immediately
  //
  generateRanges();

  //
  // Generate lengths from length expression
  //
  var lengths_array = PatternEngine.generateItems(lengthExpression);
  for (var i=0; i<lengths_array.length; i++) {
    var length = Util.number(lengths_array[i]);
    lengths.push(length);
  }

  // Public Members
  /*
   * setter and getter for name attibute
   */
  this.name = function(newName) {
    if (newName === undefined) return name;
    name = newName;
  }

  /*
   * setter and getter for type attibute
   */
  this.type = function(newType) {
    if (newType === undefined) return type;
    type = newType;
  }

  /*
   * setter and getter for motifs attibute
   */
  this.motifs = function(newMotifs) {
    if (newMotifs === undefined) return motifs;
    motifs = newMotifs;
  }

  this.isForValidation = function() {
    return isForValidation;
  }

  /*
   * setter and getter for ranges attibute
   */
  this.ranges = function(newRanges) {
    if (newRanges === undefined) return ranges;
    ranges = newRanges;
  }

  this.lengths = function(){
    return lengths;
  }

  /*
   * return expression of the feature ranges
   * @return Hash
   */
  this.rangeExpression = function(){
    return rangeExpression;
  }

  /*
   * return expression of the feature lengths
   * @return Hash
   */
  this.lengthExpression = function(){
    return lengthExpression;
  }

  /*
   * Set range expression and recompile ranges
   * @args rangeExpression String
   */
  this.compileRanges = function(newRangeExpression) {
    rangeExpression = newRangeExpression;
    generateRanges();
  }

  /*
   * build hash from feature properties
   * @return Hash
   */
  this.hashify = function(){
    return {
      name: name,
      type: type,
      rangeExpression: rangeExpression,
      range_expression: rangeExpression,
      ranges: ranges,
      motifs: motifs
    }
  }

  /*
   * Adjust feature offset by specified index
   * @args adjustmentIndex Number
   */
  this.adjustBy = function(adjustmentIndex) {
    Util.iterate(ranges, function(range){
      return range.adjustBy(adjustmentIndex);
    }, {save:true});
    return ranges;
  }

 /*
   * Match with feature ranges
   * @args start Number
   * @args finish Number
   * @return Boolean
   */
  var existOnRanges = function(start, finish, startResult, finishResult, proteinMode, matchStart, matchFinish, reduceStart, reduceFinish) {
    if (matchStart === undefined) matchStart = true;
    if (matchFinish === undefined) matchFinish = true;

    if (start > finish && matchStart && matchFinish) return false;
    var exist = false;
    Util.iterate(ranges, function(range){
      var rangeHash = range.hashify();
      var assignResult = function(){
        exist = true;
        // Assign result
        matched = {
          start:{
            index:start,
            from:start,
            to:start + startResult[0].length - 1,
            motif:startResult[0]
          },
          finish:{
            index:finish,
            from:finish - finishResult[0].length + 1,
            to:finish,
            motif:finishResult[0]
          },
          range:range,
          length:finish-start+1
        };

        if (proteinMode) {
          matched.start.index = Math.floor(matched.start.index/3);
          matched.start.from = Math.floor(matched.start.from/3);
          matched.start.to = Math.floor(matched.start.to/3);
          matched.finish.index = Math.floor(matched.finish.index/3);
          matched.finish.from = Math.floor(matched.finish.from/3);
          matched.finish.to = Math.floor(matched.finish.to/3);
        }
      }

      if (matchStart && matchFinish) {
        if (rangeHash.start + reduceStart == start && rangeHash.finish - reduceFinish == finish) {
          assignResult();
          return Util.stopIterate();
        }
      } else if (matchStart && rangeHash.start + reduceStart  == start) {
        assignResult();
        return Util.stopIterate();
      } else if (matchFinish && rangeHash.finish - reduceFinish == finish) {
        assignResult();
        return Util.stopIterate();
      }
    });
    return exist;
  }


  /*
   * Match feature with sequence
   * @args sequence Sequence
   * @return Boolean
   */
  var matchWith = function(sequence, matchStart, matchFinish, reduceStart, reduceFinish) {
    if (matchStart === undefined) matchStart = true;
    if (matchFinish === undefined) matchFinish = true;
    if (reduceStart === undefined) reduceStart = 0;
    if (reduceFinish === undefined) reduceFinish = 0;

    var startExpression = PatternEngine.strRegex(motifs.start, reduceStart, reduceFinish),
        finishExpression = PatternEngine.strRegex(motifs.finish, reduceStart, reduceFinish),
        // matcher = new RegExp("(" + startExpression + ").*(" + finishExpression + ")", "g");
        startMatcher = new RegExp(startExpression, "g"),
        finishMatcher = new RegExp(finishExpression, "g");
    var body = sequence.nucleotide();
    var startResults = [],
        finishResults = [];

    var startResult = startMatcher.exec(body),
        finishResult = finishMatcher.exec(body);

    while (startResult != null) {
      startResults.push(startResult);
      startResult = startMatcher.exec(body);
      if (startResult != null) {
        startMatcher.lastIndex = startResult.index+1;
      }
    }

    while (finishResult != null) {
      finishResults.push(finishResult);
      finishResult = finishMatcher.exec(body);
      if (finishResult != null) {
        finishMatcher.lastIndex = finishResult.index+1;
      }
    }

    if (matchStart && matchFinish) {
      if (startResults.length == 0 || finishResults.length == 0) return false;
    } else if (matchStart) {
      if (startResults.length == 0) return false;
    } else if (matchFinish) {
      if (finishResults.length == 0) return false;
    }

    // console.log(name)
    for (var i=0; i<startResults.length; i++) {
      for (var j=0; j<finishResults.length; j++) {
        var start = startResults[i].index,
            finish = finishResults[j].index + finishResults[j][0].length - 1;
        // console.log(start + " " + finish);
        if (existOnRanges(start, finish, startResults[i], finishResults[j], sequence.inProteinMode(), matchStart, matchFinish, reduceStart, reduceFinish)) return true;
      }
    }
    return false;
  }

  /*
   * Match feature with sequence or sequence group
   * @args input Sequence
   * @args reduceStart Number
   * @args reduceFinish Number
   * @return Boolean
   */
  this.match = function(input, reduceStart, reduceFinish) {
    if (input.constructor == Sequence) {
      return matchWith(input, true, true, reduceStart, reduceFinish);
    }
  }

  /*
   * return matched information
   * @return Hash
   */
  this.matched = function() {
    return matched;
  }


  this.startMatched = function(input, reduceStart, reduceFinish) {
    if (input.constructor == Sequence) {
      matchWith(input, true, false, reduceStart, reduceFinish);
      return matched;
    }
  }

  this.finishMatched = function(input, reduceStart, reduceFinish) {
    if (input.constructor == Sequence) {
      matchWith(input, false, true, reduceStart, reduceFinish);
      return matched;
    }
  }

  /*
   * Check the feature is disabled or not
   * @return Boolean
   */
  this.disabled = function() {
    return disabled;
  }

  /*
   * Enable feature
   */
  this.enable = function() {
    disabled = false;
  }

  /*
   * Disable feature
   */
  this.disable = function() {
    disabled = true;
  }
}