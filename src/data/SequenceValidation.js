function SequenceValidation(){}

SequenceValidation.hasFinishMotif = function(sequenceBody, feature) {
  var motif = feature.motifs().finish,
      matcher = new RegExp(PatternEngine.strRegex(motif), "g");
  return matcher.exec(sequenceBody) != null;
}

SequenceValidation.hasStartMotif = function(sequenceBody, feature) {
  var motif = feature.motifs().start,
      matcher = new RegExp(PatternEngine.strRegex(motif), "g");
  return matcher.exec(sequenceBody) != null;
}

SequenceValidation.correctStartMotifPosition = function(sequenceBody, feature){
  var motif = feature.motifs().start,
      matcher = new RegExp(PatternEngine.strRegex(motif), "g")
      ranges = feature.ranges();

  var matchWithRanges = function(index){
    for (var i=0; i<ranges.length; i++) {
      var range = ranges[i];
      // console.log(feature.name() + " " + range.start() + " " + index);
      if (range.start() == index) return true;
    }
    return false;
  }

  do {
    var result = matcher.exec(sequenceBody);
    if (result != null) {
      if (matchWithRanges(result.index)) return true;
      matcher.lastIndex = result.index + 1;
    }
  } while (result != null);
  return false;
}

SequenceValidation.correctFinishMotifPosition = function(sequenceBody, feature){
  var motif = feature.motifs().finish,
      matcher = new RegExp(PatternEngine.strRegex(motif), "g")
      ranges = feature.ranges();

  var matchWithRanges = function(finishIndex){
    for (var i=0; i<ranges.length; i++) {
      var range = ranges[i];
      if (range.finish() == finishIndex) return true;
    }
    return false;
  }

  do {
    var result = matcher.exec(sequenceBody);
    if (result != null) {
      if (matchWithRanges(result.index + result[0].length-1)) return true;
      matcher.lastIndex = result.index + 1;
    }
  } while (result != null);
  return false;
}

SequenceValidation.exactlyMatch = function(sequenceBody, feature){
  //console.log(feature.name() + " " + SequenceValidation.correctStartMotifPosition(sequenceBody, feature) + " " + SequenceValidation.correctFinishMotifPosition(sequenceBody, feature))
  return SequenceValidation.correctStartMotifPosition(sequenceBody, feature) && SequenceValidation.correctFinishMotifPosition(sequenceBody, feature);
}

SequenceValidation.tooLong = function(sequenceBody, feature){
  var correctStartMotifPosition = SequenceValidation.correctStartMotifPosition(sequenceBody, feature);
  var correctFinishMotifPosition = SequenceValidation.correctFinishMotifPosition(sequenceBody, feature);

  var hasFinishMotif = SequenceValidation.hasFinishMotif(sequenceBody, feature);
  var hasStartMotif = SequenceValidation.hasStartMotif(sequenceBody, feature);
  if (!hasFinishMotif || !hasStartMotif) return false;

  var sequence = new Sequence({body:sequenceBody});

  var startMatched = feature.startMatched(sequence);
  var startRange = startMatched.range;
  var finishMotifAfterCorrectPosition = startMatched.finish.index > startRange.finish();

  var finishMatched = feature.finishMatched(sequence);
  var finishRange = finishMatched.range;
  var startMotifBeforeCorrectPosition = finishMatched.start.index < finishRange.start();

  var firstCondition = correctStartMotifPosition && finishMotifAfterCorrectPosition;
  var secondCondition = correctFinishMotifPosition && startMotifBeforeCorrectPosition;

  // console.log(firstCondition + " " + secondCondition)
  if (!firstCondition && !secondCondition) {
    var startMatcher = PatternEngine.buildRegex(feature.motifs().start),
    finishMatcher = PatternEngine.buildRegex(feature.motifs().finish);
    do {
      var startResult = startMatcher.exec(sequenceBody);
      finishMatcher.lastIndex = null;
      do {
        var finishResult = finishMatcher.exec(sequenceBody);
        for (var i=0; i<feature.ranges().length; i++) {
          var range = ranges[i];
          // console.log(startResult.index + " " + finishResult.index);
          // console.log(range.hashify());
          try {
            if (startResult.index < range.start() && finishResult.index > range.finish()) return true;
          } catch(e) {
            continue;
          }
        }
        if (finishResult != null) finishResult.lastIndex = finishResult.index + 1;
      } while (finishResult != null)
      if (startResult != null) startResult.lastIndex = startResult.index + 1;
    } while(startResult != null);
  }

  return firstCondition || secondCondition;
}

SequenceValidation.tooShort = function(sequenceBody, feature){
  var correctStartMotifPosition = SequenceValidation.correctStartMotifPosition(sequenceBody, feature);
  var correctFinishMotifPosition = SequenceValidation.correctFinishMotifPosition(sequenceBody, feature);

  var hasFinishMotif = SequenceValidation.hasFinishMotif(sequenceBody, feature);
  var hasStartMotif = SequenceValidation.hasStartMotif(sequenceBody, feature);
  if (!hasFinishMotif || !hasStartMotif) return false;

  var sequence = new Sequence({body:sequenceBody});
  feature.match(sequence);

  var finishMotifBeforeCorrectPosition = false;
  var reduceFinish = 0;
  do {
    var startMatched = feature.startMatched(sequence, 0, reduceFinish);
    var startRange = startMatched.range;
    finishMotifBeforeCorrectPosition = startMatched.finish.index < startRange.finish();
    if (startMatched.finish.index == startRange.finish()) break;
    reduceFinish++;
  } while (startMatched.finish.index == -1 || !finishMotifBeforeCorrectPosition);

  var finishMatched = feature.finishMatched(sequence);
  var finishRange = finishMatched.range;
  var startMotifAfterCorrectPosition = finishMatched.start.index > finishRange.start();

  var firstCondition = correctStartMotifPosition && finishMotifBeforeCorrectPosition;
  var secondCondition = correctFinishMotifPosition && startMotifAfterCorrectPosition;

  if (!firstCondition && !secondCondition) {
    var startMatcher = PatternEngine.buildRegex(feature.motifs().start),
    finishMatcher = PatternEngine.buildRegex(feature.motifs().finish);
    do {
      var startResult = startMatcher.exec(sequenceBody);
      finishMatcher.lastIndex = null;
      do {
        var finishResult = finishMatcher.exec(sequenceBody);
        for (var i=0; i<feature.ranges().length; i++) {
          var range = ranges[i];
          try {
            if (startResult.index > range.start() && finishResult.index < range.finish()) return true;
          } catch(e) {
            continue;
          }
        }
        if (finishResult != null) finishResult.lastIndex = finishResult.index + 1;
      } while (finishResult != null)
      if (startResult != null) startResult.lastIndex = startResult.index + 1;
    } while(startResult != null);
  }

  return firstCondition || secondCondition;
}

SequenceValidation.novel = function(sequenceBody, feature) {
  var firstCondition = SequenceValidation.correctStartMotifPosition(sequenceBody, feature) && !SequenceValidation.correctFinishMotifPosition(sequenceBody, feature);
  var secondCondition = !SequenceValidation.correctStartMotifPosition(sequenceBody, feature) && SequenceValidation.correctFinishMotifPosition(sequenceBody, feature);
  return firstCondition || secondCondition;
}

SequenceValidation.partiallyMissing = function(sequenceBody, feature) {
  
}