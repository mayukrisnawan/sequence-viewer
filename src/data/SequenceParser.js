/*
 * DNA Sequence Parser
 */
function SequenceParser(){}

/*
 * Read from sequence file
 * @return Sequence
 * @args options Object
 */
SequenceParser.read = function(options){
  var content = options.content ? options.content : "";
  var type = options.type ? options.type : "fasta";
  var multiple = options.multiple ? options.multiple : false;
  
  if (type == "fasta") {
    return FastaFileParser.read(content, multiple);
  }
}