/*
 * Single Sequence Viewer Class
 * act as initializer for jQuery wrapper
 */
function SingleSequenceViewer(signedTarget, signedOptions){
  var options = {}
  var $target = $(signedTarget);
  var viewer = this;
  var process = {};

  var constructOptions = function(){
    options = {
      fasta:{},
      // cut:{},
      ambiguity:{},
      features:[],
      featureAdjustment:0
    }

    if (!signedOptions["fasta"]) throw("Fasta file not available");
    if (signedOptions["fasta"].constructor == String) {
      options.fasta.url = signedOptions["fasta"];
    } else {
      deepCopy(signedOptions["fasta"], options.fasta);
    }

    // if (signedOptions["cut"] !== undefined) if (signedOptions["cut"].constructor == String) {
    //   options.cut.url = signedOptions["cut"];
    // } else {
    //   deepCopy(signedOptions["cut"], options.cut);
    // }

    if (signedOptions["features"] !== undefined) if (signedOptions["features"].constructor == String) {
      options.features = { url: signedOptions.features };
    } else {
      deepCopy(signedOptions.features, options.features);
    }

    if (signedOptions["ambiguity"] !== undefined) if (signedOptions["ambiguity"].constructor == String) {
      options.ambiguity = { url: signedOptions.ambiguity };
    } else {
      deepCopy(signedOptions.ambiguity, options.ambiguity);
    }

    if (signedOptions.featureAdjustment) options.featureAdjustment = signedOptions.featureAdjustment;
  }
  constructOptions();

  

  var elements = new UI.ElementManager($target, {viewer:viewer});
  var sequence = null;
  var lastSearchIndex = 0;
  var featureManager = new FeatureManager();
  var templateManager = new UI.TemplateManager();

  var configs = {
    sequence_box_size : 20,
    feature_box_size : 14,
    highlight_color : "#2DABFF",
    hover_color : "#FFEE30"
  }

  var ambiguityCorrections = {};

  /*
    Site Selection Functionality
  */
  var selectionMode = "multiple";
  var selectedSites = [];

  this.singleSelectionMode = function(){
    selectionMode = "single";
  }

  this.multipleSelectionMode = function(){
    selectionMode = "multiple";
  }

  this.selectionMode = function(){
    return selectionMode;
  }

  this.selectedSites = function(){
    return selectedSites;
  }

  this.clearSiteSelection = function(){
    selectedSites = [];
    this.$el("sequence_body").find(".sv_sequence_box[data-selected='true']").each(function(){
      $(this).click();
    });
    // console.log(selectedSites);
    SequenceSelectionModule.notifySelectedSites(viewer);
  }

  this.selectSite = function($box){
    SequenceSelectionModule.selectSite(viewer, $box);
    SequenceSelectionModule.notifySelectedSites(viewer);
  }

  this.unselectSite = function($box){
    SequenceSelectionModule.unselectSite(viewer, $box);
    SequenceSelectionModule.notifySelectedSites(viewer);
  }

  this.deleteSelectedSites = function(){
    var result = confirm("Are you sure to delete selected sites from sequence?");
    if (result === true) {
      SequenceSelectionModule.deleteSelectedSites(viewer);
      this.notifySequenceEdit();
    }
  }
  /*
    /Site Selection Functionality
  */

  /*
   * Return Element Manager of the viewer
   * @return UI.ElementManager
   */
  this.elementManager = function(){
    return elements;
  }

  /*
   * Return Template Manager of the viewer
   * @return UI.TemplateManager
   */
  this.templateManager = function(){
    return templateManager;
  }

  /*
   * Return Feature Manager of the viewer
   * @return FeatureManager
   */
  this.featureManager = function(){
    return featureManager;
  }

  /*
   * Return sequence object of the viewer
   * @return Sequence
   */
  this.sequence = function(){
    return sequence;
  }

  /*
   * Get sequene viewer configuration
   * @args name String
   * @return Hash|Object
   */
  this.configs = function(name){
    if (name === undefined) return configs;
    return configs[name];
  }

  /*
   * Element selector helper
   * @return jQuerySelector
   */
  this.$el = function(name){
    var el = elements.get(name);
    if (el == false) throw "Element " + name + " not found";
    return el.$el();
  }

  /*
   * Element selector helper
   * @return jQuerySelector
   */
  this.dom = function(name){
    return elements.get(name).dom();
  }

  /*
   * Load sequence
   * @args options Hash
   */
  this.loadSequenceFrom = function(sequenceOptions, callback){
    var passedCallback = sequenceOptions.success;
    sequenceOptions.success = function(response){
      sequence = FastaFileParser.read(response);
      callback();
      if (passedCallback) passedCallback();
    }
    $.ajax(sequenceOptions);
  }

  /*
   * Load features
   * @args options Hash
   * @args callback Function
   */
  this.loadFeaturesFrom = function(featuresOptions, callback){
    // return immediately when feature options defined as an array
    if (featuresOptions.constructor == Array) {
      callback();
      return false;
    }

    var passedCallback = featuresOptions.success;
    featuresOptions.dataType = "json";
    featuresOptions.success = function(response){
      options.features = response;
      callback();
      if (passedCallback) passedCallback();
    }
    featuresOptions.complete = function(){
      // viewer.$el("btn_show_feature_manager").removeAttr("disabled");
      // viewer.$el("btn_hide_feature_manager").removeAttr("disabled");
      //viewer.notifyLeft("");
    }

    viewer.$el("btn_show_feature_manager").attr("disabled", "disabled");
    viewer.$el("btn_hide_feature_manager").attr("disabled", "disabled");
    viewer.notifyLeft("Loading sequence features..");
    $.ajax(featuresOptions);
  }

  /*
   * Load the ruler of the viewer
   */
  this.loadRuler = function(){
    var $container = this.$el("sequence_body");
    SequenceBodyModule.loadRuler(viewer, $container, sequence);
  }


  this.hideRuler = function(){
    this.$el("sequence_body").find(".sv_ruler").hide();
  }

  this.showRuler = function(){
    this.$el("sequence_body").find(".sv_ruler").show();
  }

  /*
   * Load sequence body
   */
  this.loadSequenceBody = function(){
    var $container = this.$el("sequence_body");
    var $proteinPreviewContainer = this.$el("protein_preview");
    if (!viewer.inProteinMode()) {
      $proteinPreviewContainer.show();
      SequenceBodyModule.loadProteinPreview(viewer, $proteinPreviewContainer, sequence, configs.sequence_box_size);
    } else {
      $proteinPreviewContainer.hide();
    }
    SequenceBodyModule.loadSequenceBodySVG(viewer, $container, sequence, configs.sequence_box_size);
    this.slideTo(0);
  }

  /*
   * Load sequence header
   */
  this.loadSequenceHeader = function(){
    this.$el("sequence_header").html(sequence.header());
  }

  /*
   * Load features to the viewer
   */
  this.loadFeatures = function(){
    SequenceFeatureModule.renderFeatureTo(this);
  }

  this.showFeatures = function(){
    this.$el("feature_layers").show(200);
  }

  this.hideFeatures = function(){
    this.$el("feature_layers").hide(200);
  }

  /*
   * Notify something in the right status bar
   * @args content String
   */ 
  this.notifyRight = function(content){
    this.$el("right_status").html(content);
  }

  /*
   * Notify something in the left status bar
   * @args content String
   */ 
  this.notifyLeft = function(content){
    this.$el("left_status").html(content);
  }

  /*
   * Flush notification of the viewer
   */
  this.flushNotification = function(){
    this.notifyLeft("");
    this.notifyRight("");
  }

  /*
   * Slide sequence body to specified index
   * @args sequenceIndex Number
   */
  this.slideTo = function(sequenceIndex){
    this.$el("site_input").html(sequenceIndex+1);
    this.$el("sequence_body").scrollLeft(sequenceIndex * configs.sequence_box_size);
    viewer.$el("site_input").val(sequenceIndex+1);
  }

  /*
   * Set viewer to protein mode or nucleotide mode
   * @args protein Boolean
   */
  this.setProteinMode = function(protein) {
    this.flushProcess();
    if (protein) {
      sequence.proteinize();
      viewer.$el("ambiguity_editor").hide(0);
      viewer.$el("btn_hide_ambiguity_editor").hide(0);
      viewer.$el("btn_show_ambiguity_editor").hide(0);
    } else {
      sequence.unproteinize();
      viewer.$el("ambiguity_editor").hide(0);
      viewer.$el("btn_hide_ambiguity_editor").hide(0);
      viewer.$el("btn_show_ambiguity_editor").show(0);
    }
    Ambiguity.check(viewer);
    this.loadSequenceBody();
    this.loadRuler();
    this.flushNotification();
    this.loadFeatures();
    this.clearSiteSelection();
  }

  this.inProteinMode = function() {
    return sequence.inProteinMode();
  }

  /*
   * Search with specified pattern
   * @args pattern String
   */
  this.search = function(pattern){
    var result = sequence.search(pattern);
    var message = "";
    if (result.index != -1) {
      SingleSVModules.highlightSequence(this, result.start, result.finish);
      message = "Found at " + (result.start + 1);
      if (result.start != result.finish) message += " to " + (result.finish + 1);
    } else {
      SingleSVModules.unhighlightSequence(this);
      this.slideTo(0);
      message = "<font color='red'>Search with query '" + pattern + "' has no result</font>";
    }
    this.notifyLeft("<i>" + message + "</i>");
  }

  /*
   * Add ambiguity correction to the sequence
   * @args correction Hash
   */
  this.ambiguityCorrection = function(correction) {
    ambiguityCorrections[correction.index] = correction.code;
    sequence.edit(correction.index, correction.code);
    SequenceQualityModule.validate(viewer, sequence);
    viewer.softReload();
    viewer.slideTo(correction.index);
    this.notifySequenceEdit();
  }

  this.notifySequenceEdit = function(){
    if (signedOptions.sequence_edited === undefined) return false;
    signedOptions.sequence_edited(viewer);
  }

  /*
   * Return ambiguity corrections
   * @return Hash
   */
  this.ambiguityCorrections = function(){
    return ambiguityCorrections;
  }

  this.attachProcess = function(name, fn){
    if (process[name] !== undefined) return false;
    process[name] = fn;
    // console.log(name + " attached");
  }

  this.processRunning = function(name){
    return process[name] === undefined ? false : true;
  }

  this.terminateProcess = function(name){
    // console.log("terminating " + name);
    if (process[name] == undefined) return false; 
    process[name] = undefined;
    // console.log(name + " terminated");
  }

  this.flushProcess = function(){
    process = {};
    // console.log("processes flushed");
    // console.log(viewer.processRunning("bind_feature_index"));
  }

  /*
   * Initialize the sequence viewer
   */
  this.init = function(){
    this.loadSequenceFrom(options.fasta, function(){
      SingleSVModules.createContainer(viewer, $target);
      SingleSVModules.registerElements(viewer);
      SingleSVModules.registerEvents(viewer);

      Ambiguity.registerElements(viewer);
      Ambiguity.registerEvents(viewer);
      Ambiguity.check(viewer);

      viewer.loadSequenceBody();
      viewer.loadSequenceHeader();
      viewer.loadRuler();
      if (options.features !== undefined) viewer.loadFeaturesFrom(options.features, function(){
        Util.iterate(options.features, function(feature){
          var f = new Feature(feature);
          // console.log(feature)
          // console.log(f.isForValidation())
          f.adjustBy(options.featureAdjustment);
          // 
          // @note Feature Matching with UTR Feature
          // @desc Ignore UTR PART, REMEMBER!!
          // f.ignoreBy();
          //
          featureManager.attach(f);
        });
        viewer.loadFeatures();
        FeatureManagerModule.loadFeatureList(viewer);

        SequenceQualityModule.registerElements(viewer);
        SequenceQualityModule.registerEvents(viewer);
        viewer.notifyLeft("");
        SequenceQualityModule.validate(viewer, sequence);
      });
    });
  }

  this.notifyReload = function(){
    viewer.clearSiteSelection();
    viewer.notifyLeft("");
    viewer.notifyRight("Reloading viewer..");
    viewer.$el("sequence_body").html("");
  }

  this.reload = function(reload) {
    viewer.notifyReload();
    elements = new UI.ElementManager($target, {viewer:viewer});
    sequence = null;
    lastSearchIndex = 0;
    featureManager = new FeatureManager();
    templateManager = new UI.TemplateManager();
    ambiguityCorrections = {};
    process = {};

    selectionMode = "multiple";

    constructOptions();
    $target.unbind("click");

    viewer.init();
  }

  this.softReload = function(){
    viewer.flushProcess();
    viewer.notifyReload();
    this.loadSequenceBody();
    this.loadRuler();
    this.flushNotification();
    this.loadFeatures();
    this.clearSiteSelection();
  }

  // initialize viewer immediatelly
  this.init();
}