/*
 * PatternEngine Class
 * used for pattern related tasks
 */
function PatternEngine(){}

/*
 * Translate String to
 * Correct Regular Expression String
 * for example AT/GG/C/ATT => A[TG][GCA]TT
 * @args input String
 * @args reduceStart Number
 * @args reduceFinish Number
 * @return String
 */
PatternEngine.strRegex = function(input, reduceStart, reduceFinish, replacement) {
  if (reduceStart === undefined) reduceStart = 0;
  if (reduceFinish === undefined) reduceFinish = 0;
  var output = "";
  var slashIndexes = [];
  var groups = [];
  var vertexes = [];
  var matcher = new RegExp("/", "g");

  // Remove all spaces
  input = input.replace(/[ ]/g, "");

  // Remove slash at begin and end of input
  input = input.replace(/^[\//]+/, "");
  input = input.replace(/[\/]+$/, "");

  // Remove double slashes
  input = input.replace(/[\/]{2,}/g, "/");

  // Find all the slash indexes
  do {
    var result = matcher.exec(input);
    if (result != null) slashIndexes.push(result.index);
  } while (result != null);
  
  // Remove all slashes
  // input = input.replace(/\//g, "");

  // Construct slash groups
  var lastPosition = -1;
  for (var i=0; i<slashIndexes.length; i++) {
    var position = slashIndexes[i];
    if (lastPosition == -1 || (position-lastPosition > 2)) {
      // Create new group
      groups.push([position]);
    } else {
      // Append Existing Group
      groups[groups.length-1].push(position);
    }
    lastPosition = position;
  }

  // Create group vertexes based on groups
  for (var i=0; i<groups.length; i++) {
    var group = groups[i];
    var minIndex = group[0] - 1;
    var maxIndex = group[group.length-1] + 1;
    vertexes.push({
      min:minIndex,
      max:maxIndex,
      builded:false,
      build:function(input){
        var start = this.min;
        var count = this.max - this.min + 1;
        this.builded = true;
        return input.substr(start, count);
      }
    });
  }

  
  var existOnVertexes = function(index, vertexes) {
    for (var i=0; i<vertexes.length; i++) {
      var vertex = vertexes[i];
      if (vertex.min <= index && index <= vertex.max) {
        return i;
      }
    }
    return false;
  }

  var outputs = [];

  // Create output based on group vertexes
  for (var i=0; i<input.length; i++) {
    var vertexIndex = existOnVertexes(i, vertexes)
    if (vertexIndex !== false) {
      var vertex = vertexes[vertexIndex];
      if (vertex.builded) continue;
      var group = vertex.build(input);
      group = group.replace(/\//g, "");
      var output = "[" + group + "]";
    } else {
      var output = input[i];
    }
    outputs.push(output);
  }

  var result = "";
  for (var i=reduceStart; i<outputs.length-reduceFinish; i++) {
    result += outputs[i];
  }

  if (replacement !== undefined) {
    for (var i=0; i<reduceStart; i++) result = replacement + result;
    for (var i=outputs.length-reduceFinish; i<outputs.length; i++) result = result + replacement;
  }

  return result;
}

/*
 * Create regular expression instance from specified pattern
 * @args pattern String
 * @return RegExp
 */
PatternEngine.buildRegex = function(pattern) {
  pattern = PatternEngine.strRegex(pattern);
  try {
    return new RegExp(pattern, "g");
  } catch(e) {
    return false;
  }
}

/*
 * Create item string from specified pattern
 * "AAA/GGG/CCC" => ["AAA", "GGG", "CCC"]
 * @args input String
 * @return Array of String
 */
PatternEngine.generateItems = function(input) {  
  // Remove slash at begin and end of input
  input = input.replace(/^[\//]+/, "");
  input = input.replace(/[\/]+$/, "");

  // Remove double slashes
  input = input.replace(/[\/]{2,}/g, "/");

  // split string by slash
  var output = input.split("/");

  return output;
}

/*
 * Generate Range Object from expression, for example :
 * Range expression :
 * {
 *   start:"11/20",
 *   finish:"50/80"
 * }
 *
 * Result :
 * [Range(11,50), Range(20,80)]
 *
 * @args rangeExpression String
 * @return array of Range
 */
PatternEngine.generateRanges = function(rangeExpression) {
  var ranges = [];
  var startItems = PatternEngine.generateItems(rangeExpression.start.trim());
      finishItems = PatternEngine.generateItems(rangeExpression.finish.trim());
  var length = Math.max(startItems.length, finishItems.length);
  for (var i=0; i<length; i++) {
    var startItem = startItems[i],
        finishItem = finishItems[i];
    if (startItem === undefined) {
      for (var j=i-1; j>=0; j--) {
        if (startItems[j]) startItem = startItems[j];
      }
    }
    if (finishItem === undefined) {
      for (var j=i-1; j>=0; j--) {
        if (finishItems[j]) finishItem = finishItems[j];
      }
    }
    if (startItem === undefined || finishItem === undefined) continue;
    if (startItem.length == 0 || finishItem.length == 0) continue;
    var start = parseInt(startItem),
        finish = parseInt(finishItem);
    var range = new Range(start, finish);
    ranges.push(range);
  }
  return ranges;
}