/*
 * ArgumentParser Class
 * Used to simplify the arguments parsing 
 */
function ArgumentParser(){
  var rules = [];
  var selectedIndex = -1;

  /*
   * Add argument parsing rule (ArgumentRule instance)
   * @args rule ArgumentRule
   */
  this.add = function(rule){
    if (rule.constructor != ArgumentRule) return false;
    rules.push(rule);
  };

  /*
   * Add argument parsing rule (ArgumentRule instance)
   * @args rule ArgumentRule
   */
  this.index = function(){
    return selectedIndex;
  };

  /*
   * Start argument parsing with specified arguments
   * @args args array of argument
   * @return Object
   */
  this.parse = function(args) {
    if (args.length == 0) return false;
    for (var i=0; i<rules.length; i++) {
      var rule = rules[i];
      if (rule.matchWith(args)) {
        selectedIndex = i;
        return rule.createArguments(args);
      }
    }
  }
}