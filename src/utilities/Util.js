/*
 * Utility Class
 */
 function Util(){}

 /*
  * convert string to camel case
  * @args input String
  * @return String
  */
Util.camelize = function(input){
  var words = input.split("_");
  var output = "";
  for (var i=0; i<words.length; i++) {
    var word = words[i];
    if (i == 0) {
      word = word.substr(0, 1).toLowerCase() + word.substr(1, word.length);
    } else {
      word = word.substr(0, 1).toUpperCase() + word.substr(1, word.length);
    }
    output += word;
  }
  return output;
}

/*
 * convert string to underscored case
 * @args input String
 * @return String
 */
Util.uncamelize = function(input){
  var words = [];
  var word = "";
  for (var i=0; i<input.length; i++) {
    var isCapital = input[i].toUpperCase() == input[i];
    if (isCapital && i != 0) {
      words.push(word);
      word = input[i].toLowerCase(); 
    } else {
      word += input[i].toLowerCase();
    }
  }
  // push last word
  if (word.length != 0) words.push(word);
  return words.join("_");
}

Util.iterate = function(items, action, options){
  if (!options) options = {};
  for (var i = 0; i < items.length; i++) {
    var item = items[i];
    var result = item;
    // pass item attributes
    if (options.attributes) {
      var attributes = [];
      for (var attr in Object(item)) {
        attributes.push(item[attr]);
      }
      //console.log(attributes);
      result = action.apply(item, attributes);
    } else {
      result = action.call(item, item, i);
    }

    // change the content of items
    if (options.save) {
      if (result.constructor == Util.IteratorFinishFlag) {
        items[i] = result.returnValue;
      } else {
        items[i] = result;
      }
    }

    if (result !== undefined) {
      if (result.constructor == Util.IteratorFinishFlag) break;
    }
  }
}


Util.IteratorFinishFlag = function(returnValue){
  this.returnValue = null;
  if (returnValue !== undefined) {
    this.returnValue = returnValue;
  }
}

Util.stopIterate = function(returnValue){
  return new Util.IteratorFinishFlag(returnValue);
}

Util.number = function(stringInput){
  try {
    stringInput = stringInput.replace(/[^0-9]/g, "")
    return (new Number(stringInput)).valueOf();
  } catch (e) {
    return stringInput;
  }
}

Util.merge = function(){
  var results = [];
  for (var i=0; i < arguments.length; i++) {
    var data = arguments[i];
    for (var j=0; j<data.length; j++) {
      results.push(data[j]);
    }
  }
  return results;
}