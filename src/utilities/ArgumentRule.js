/*
 * ArgumentRule Class
 * Used to simplify the arguments parsing
 * and belongs to ArgumentParser Object 
 */
function ArgumentRule(_args){
  var signedArguments = _args;

  /*
   * Check the signed rules match with the given arguments
   * @return Boolean
   */
  this.matchWith = function(args) {
    var match = true;

    for (var i=0; i<signedArguments.length; i++) {
      signedArgument = signedArguments[i];
      if (signedArgument.default !== undefined) continue;
      argument = args[i];
      if (argument.constructor != signedArgument.type) {
        match = false;
        break;
      }
    }
    return match;
  }

  this.createArguments = function(args) {
    var results = {};
    for (var i=0; i<signedArguments.length; i++) {
      signedArgument = signedArguments[i];
      results[signedArgument.name] = args[i] ? args[i] : signedArgument.default;
    }
    return results;
  }
}