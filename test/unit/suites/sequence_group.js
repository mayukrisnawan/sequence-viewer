asyncTest("Sequence Group Search", function() {
  $.ajax({
    "url":"../resources/fasta_input/sequence_group_search.fasta",
    "dataType":"text",
    success:function(data){
      var sequenceGroup = FastaFileParser.read(data, true);

      // assert sequence group order
      sequenceGroup.sort();
      equal(sequenceGroup.find(0).header(), "lcl|FJ966244.1_cdsid_ACQ90234.1_gene_HA_protein_hemagglutinin_protein_id_ACQ90234.1_location_33..1751");
      equal(sequenceGroup.find(1).header(), "lcl|GQ122391.1_cdsid_ACR49188.1_gene_HA_protein_hemagglutinin_protein_id_ACR49188.1_location_1..1707");

      deepEqual(sequenceGroup.position(), {
        row:0,
        index:0,
        start:0,
        finish:0,
        length:0
      }, "First state of sequence group position");

      var simpleSearch = sequenceGroup.search("CAC/AT");
      equal(simpleSearch.index, 31, "1st Search Index");
      equal(simpleSearch.length, 4, "1st Search Length");
      equal(simpleSearch.start, 31, "1st Search Start Index");
      equal(simpleSearch.finish, 34, "1st Search Finish Index");
      equal(simpleSearch.row, 0, "1st Search Row");
      deepEqual(sequenceGroup.position(), {
        row:0,
        index:31,
        start:31,
        finish:34,
        length:4
      }, "Second state of sequence group position");

      equal(sequenceGroup.search("AATGGA/T/GACT/A").index, 309, "2nd Search");
      equal(sequenceGroup.search("AATGGA/T/GACT/A").index, 1504, "3rd Search (Almost at the end)");

      var firstJumpSearch = sequenceGroup.search("ATGGAGAAAA");
      equal(firstJumpSearch.index, 0, "1st sequence jump index");
      equal(firstJumpSearch.row, 1, "1st sequence jump row");
      deepEqual(sequenceGroup.position(), {
        row:1,
        index:0,
        start:0,
        finish:9,
        length:10
      }, "State after row jump");

      var secondJumpSearch = sequenceGroup.search("ATGAAAGTAAAA");
      equal(secondJumpSearch.index, 0, "2nd sequence jump index");
      equal(secondJumpSearch.row, 0, "2nd sequence jump row");

      var secondJumpSearch = sequenceGroup.search("ATGAAAGTAAAA");
      equal(secondJumpSearch.index, -1, "index should be same as last search");
      equal(secondJumpSearch.row, 0, "row should be same as last search");
      start();
    }
  });
});