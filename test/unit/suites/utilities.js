test("Util class", function(){
  equal(Util.camelize("a_abc_D"), "aAbcD", "should be camelized when input is a_abc_D");
  equal(Util.camelize("A_abc_e"), "aAbcE", "should be camelized when input is A_abc_e");
  equal(Util.camelize("AabcD"), "aabcD",  "should be camelized when input is AabcD");

  equal(Util.uncamelize("aAbcD"), "a_abc_d",  "should be camelized when input is aAbcD");
  equal(Util.uncamelize("aAbcE"), "a_abc_e",  "should be camelized when input is aAbcE");
  equal(Util.uncamelize("aabcD"), "aabc_d",  "should be camelized when input is aabcD");
});

test("Argument Rule Matching", function(){
  var rule = new ArgumentRule([
    {name:"var1", type:String},
    {name:"var2", type:Number},
    {name:"var3", type:Number, default:-1}
  ]);
  equal(rule.matchWith(["aa", 1, 1]), true, "Match when all arguments defined");
  equal(rule.matchWith(["aa", 1]), true, "Match when optional argument have been initialized");
  equal(rule.matchWith([1, 1]), false, "Not match when argument type is not same");
});

test("Argument Parsing", function(){
  function argumentParsing() {
    var arg = new ArgumentParser();
    arg.add(new ArgumentRule([
      {name:"var1", type:String},
      {name:"var2", type:Number},
      {name:"var3", type:Number, default:-1}
    ]));
    arg.add(new ArgumentRule([
      {name:"options", type:Object}
    ]));
    args = arg.parse(arguments);

    if (arg.index() == 0) {
      return new Number(args.var1).valueOf() + args.var2 + args.var3;
    } else if (arg.index() == 1) {
      return new Number(args.options["var1"]).valueOf() + args.options["var2"] + args.options["var3"];
    } else {
      return false;
    }
  }

  equal(argumentParsing(), false);
  equal(argumentParsing("1",2,3), 6);
  equal(argumentParsing("1",2), 2);
  equal(argumentParsing({
    "var1":1,
    "var2":2,
    "var3":3
  }), 6);
});