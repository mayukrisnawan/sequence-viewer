test("Validate too short", function(){
  var feature = new Feature({
    "name":"HA2 chain",
    "type":"Feature Type 1",
    "length_expression":"10/11",
    "range_expression":{
      "start":"1/2",
      "finish":"10/12"
    },
    "motifs":{
      "start":"GG A/T",
      "finish":"TGCA"
    },
    "isForValidation":true
  });
  feature.adjustBy(-1);

  var sequenceBody1 = "AGGACTGCA";
  var sequenceBody2 = "AGGCCCCCTGCA";
  var sequenceBody3 = "CCCCAGGATGCA";
  var sequenceBody4 = "GGATGCA";

  equal(SequenceValidation.tooShort(sequenceBody1, feature), true);
  equal(SequenceValidation.tooShort(sequenceBody2, feature), false);
  equal(SequenceValidation.tooShort(sequenceBody3, feature), true);
  equal(SequenceValidation.tooShort(sequenceBody4, feature), true);
  
});