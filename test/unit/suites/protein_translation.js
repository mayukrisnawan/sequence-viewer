test("Sequence Protein Translation", function() {
  // 1st Translatation (without gap)
  $.ajax({
    "url":"../resources/fasta_input/single.fasta",
    "async":false,
    "dataType":"text",
    success:function(data){
      var sequence = FastaFileParser.read(data);
      var nucleotideSequence = FastaFileParser.read(data);
      var proteinFrom = sequence.protein();
      $.ajax({
        "url":"../resources/fasta_output/single_protein.fasta",
        "async":false,
        success:function(data){
          var sequenceProtein = FastaFileParser.read(data);
          equal(proteinFrom, sequenceProtein.body(), "Should be converted to protein form");

          sequence.proteinize();
          equal(sequence.body(), sequenceProtein.body(), "Should be converted to protein when proteinized");

          sequence.unproteinize();
          equal(sequence.body(), nucleotideSequence.body(), "Should be converted to nucleotide when unproteinized");
        }
      });
    }
  });

  // 2nd Translatation (with gaps)
  $.ajax({
    "url":"../resources/fasta_input/single_with_gaps.fasta",
    "async":false,
    "dataType":"text",
    success:function(data){
      var sequence = FastaFileParser.read(data);
      var nucleotideSequence = FastaFileParser.read(data);
      var proteinFrom = sequence.protein();
      $.ajax({
        "url":"../resources/fasta_output/single_with_gaps_protein.fasta",
        "async":false,
        success:function(data){
          var sequenceProtein = FastaFileParser.read(data);
          equal(proteinFrom, sequenceProtein.body(), "Should be converted to protein form");

          sequence.proteinize();
          equal(sequence.body(), sequenceProtein.body(), "Should be converted to protein when proteinized");

          sequence.unproteinize();
          equal(sequence.body(), nucleotideSequence.body(), "Should be converted to nucleotide when unproteinized");
        }
      });
    }
  });
});

test("Sequence Group Protein Translation", function(){
  $.ajax({
    "url":"../resources/fasta_input/multiple.fasta",
    "async":false,
    "dataType":"text",
    success:function(data){
      var sequenceGroup = FastaFileParser.read(data, true);
      var nucleotideSequenceGroup = FastaFileParser.read(data, true);
      var result = sequenceGroup.proteinize();
      $.ajax({
        "url":"../resources/fasta_output/multiple_protein.fasta",
        "async":false,
        success:function(data){
          var proteinSG = FastaFileParser.read(data, true);
          equal(sequenceGroup.find(0).body(), proteinSG.find(0).body(), "1st sequece should be proteinized");
          equal(sequenceGroup.find(1).body(), proteinSG.find(1).body(), "2nd sequece should be proteinized");
          deepEqual(result, sequenceGroup.all().map(function(sequence){
            return sequence.body();
          }));

          result = sequenceGroup.unproteinize();
          equal(sequenceGroup.find(0).body(), nucleotideSequenceGroup.find(0).body(), "1st sequece should be unproteinized");
          equal(sequenceGroup.find(1).body(), nucleotideSequenceGroup.find(1).body(), "2nd sequece should be unproteinized");
          deepEqual(result, sequenceGroup.all().map(function(sequence){
            return sequence.body();
          }));
        }
      });
    }
  });
});