test("Validate too long", function(){
  var feature = new Feature({
    "name":"HA2 chain",
    "type":"Feature Type 1",
    "length_expression":"10/11",
    "range_expression":{
      "start":"5/6",
      "finish":"14/16"
    },
    "motifs":{
      "start":"GG A/T",
      "finish":"TGCA"
    },
    "isForValidation":true
  });
  feature.adjustBy(-1);

  var sequenceBody1 = "1234AGGACCCCCCTGCA";
  var sequenceBody2 = "1234AGGCCCCCTGCA";
  var sequenceBody3 = "AGGA1234CCCCTGCA";
  var sequenceBody4 = "GGA1234AXXXCCCCTGCA";

  equal(SequenceValidation.tooLong(sequenceBody1, feature), true);
  equal(SequenceValidation.tooLong(sequenceBody2, feature), false);
  equal(SequenceValidation.tooLong(sequenceBody3, feature), true);
  equal(SequenceValidation.tooLong(sequenceBody4, feature), true);
  
});