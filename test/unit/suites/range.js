test("Range intersection", function(){
  var a = new Range(1, 10),
      b = new Range(2, 11),
      c = new Range(11, 15),
      d = new Range(110, 150),
      e = new Range(112, 160);

  equal(a.intersect(b), true, a.hashify().start + "-" + a.hashify().finish + " " + b.hashify().start + "-" + b.hashify().finish);
  equal(b.intersect(a), true, b.hashify().start + "-" + b.hashify().finish + " " + a.hashify().start + "-" + a.hashify().finish);

  equal(a.intersect(c), false, a.hashify().start + "-" + a.hashify().finish + " " + c.hashify().start + "-" + c.hashify().finish);
  equal(c.intersect(a), false, c.hashify().start + "-" + c.hashify().finish + " " + a.hashify().start + "-" + a.hashify().finish);

  equal(d.intersect(e), true, d.hashify().start + "-" + d.hashify().finish + " " + e.hashify().start + "-" + e.hashify().finish);
  equal(e.intersect(d), true, e.hashify().start + "-" + e.hashify().finish + " " + d.hashify().start + "-" + d.hashify().finish);

  equal(e.intersect(c), false, e.hashify().start + "-" + e.hashify().finish + " " + c.hashify().start + "-" + c.hashify().finish);
  equal(c.intersect(e), false, c.hashify().start + "-" + c.hashify().finish + " " + e.hashify().start + "-" + e.hashify().finish);
});