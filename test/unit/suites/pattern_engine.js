test("Pattern Engine Regex Builder", function(){
  equal(PatternEngine.strRegex("AA/TGG"), "A[AT]GG");
  equal(PatternEngine.strRegex("AA/T/C/GGG/C"), "A[ATCG]G[GC]");
  equal(PatternEngine.strRegex("//A/T//GG///"), "[ATG]G");
  equal(PatternEngine.strRegex("//A/T//GG///", 1, 0), "G", "reducement");
  equal(PatternEngine.strRegex("//A/T//GG///", 1, 0, "?"), "?G", "reducement with replacement");
});

test("Pattern Engine Items Builder", function(){
  deepEqual(PatternEngine.generateItems("AA/TGG"), [
    "AA",
    "TGG"
  ]);
  deepEqual(PatternEngine.generateItems("AA/T/C/GGG/C"), [
    "AA", "T", "C", "GGG", "C"
  ]);
  deepEqual(PatternEngine.generateItems("//A/T//GG///"), [
    "A", "T", "GG"
  ]);
});

test("Pattern Engine Ranges Builder", function(){
  // 1st Expression
  var firstRangeExpression = {
    start:"11/20",
    finish:"50/90"
  }
  var firstRanges = PatternEngine.generateRanges(firstRangeExpression);
  deepEqual(firstRanges[0].hashify(), {
    start:11,
    finish:50
  }, "First Expression, first range");
  deepEqual(firstRanges[1].hashify(), {
    start:20,
    finish:90
  }, "First Expression, second range");

  // 2nd Expression
  var firstRangeExpression = {
    start:"//11//20//50//",
    finish:"  //50//90  "
  }
  var secordRanges = PatternEngine.generateRanges(firstRangeExpression);
  deepEqual(secordRanges[0].hashify(), {
    start:11,
    finish:50
  }, "Second Expression, first range");
  deepEqual(secordRanges[1].hashify(), {
    start:20,
    finish:90
  }, "Second Expression, second range");

  // Not equal expression length
  var neExp = {
    start:"2",
    finish:"4/5"
  }
  var neRanges = PatternEngine.generateRanges(neExp);
  deepEqual(neRanges[0].hashify(), {
    start:2,
    finish:4
  }, "Not equal ranges count#1");
  deepEqual(neRanges[1].hashify(), {
    start:2,
    finish:5
  }, "Not equal ranges count#2");

  // empty expression
  var emptyExp = {
    start:"",
    finish:"2/3"
  }
  var emptyRanges = PatternEngine.generateRanges(emptyExp);
  equal(emptyRanges.length, 0, "Empty expression");
});