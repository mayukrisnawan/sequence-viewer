asyncTest("Sequence Search", function() {
  $.ajax({
    "url":"../resources/fasta_input/single.fasta",
    "dataType":"text",
    success:function(data){
      var sequence = FastaFileParser.read(data);
      var firstSearch = sequence.search("CTT");
      equal(firstSearch.index, 4, "1st Result");
      equal(firstSearch.length, 3, "1st Result Length");
      equal(firstSearch.start, 4, "1st Result Start Index");
      equal(firstSearch.finish, 6, "1st Result Finish Index");
      equal(sequence.position().index, 4, "1st change of the current sequence index");
      equal(sequence.search("CTT").index, 50, "2nd Result");
      equal(sequence.search("CTT").index, 200, "3rd Result");
      equal(sequence.search("CTT").index, 209, "4th Result");
      equal(sequence.search("CTT").index, 256, "5th Result");
      equal(sequence.search("CTT").index, 329, "6th Result (Match with end part of sequence)");
      equal(sequence.search("CTT").index, 4, "Search should return to starting index");
      equal(sequence.search("CAG").index, 7, "Nearest continuous search");

      var motifSearch = sequence.search("TCA/T");
      equal(motifSearch.index, 10, "1st motif search");
      equal(motifSearch.length, 3, "1st motif search length");
      equal(sequence.search("TCA/T").index, 34, "2nd motif search");
      equal(sequence.search("TCA/T").index, 96, "3rd motif search");

      var complexMotfSearch = sequence.search("TC/A/TTT/G");
      equal(complexMotfSearch.index, 199, "1st Complex motif search");
      equal(complexMotfSearch.length, 4, "1st Complex motif search length");
      equal(sequence.search("TC/A/TTT/G").index, 232, "2nd Complex motif search");
      equal(sequence.search("xxx").index, -1, "Not Found");
      equal(sequence.search("ACA").index, 66, "1st search after not found");
      equal(sequence.search("CTT").index, 200, "2nd search after not found");
      start();
    }
  });
});