test("Feature Properties", function(){
  var feature = new Feature({
    name:"HA1 Chain",
    type:"mat1_peptide",
    range_expression:{
      start:"1/5",
      finish:"21/25"
    },
    motifs:{
      start:"ATG/C",
      finish:"GCC/G"
    }
  });
  equal(feature.name(), "HA1 Chain");
  equal(feature.type(), "mat1_peptide");
  deepEqual(feature.ranges()[0].hashify(), {
    start:1,
    finish:21
  });
  deepEqual(feature.ranges()[1].hashify(), {
    start:5,
    finish:25
  });

  // Adjust Feature Index
  feature.adjustBy(-1);
  deepEqual(feature.ranges()[0].hashify(), {
    start:0,
    finish:20
  });
  deepEqual(feature.ranges()[1].hashify(), {
    start:4,
    finish:24
  });
});

test("Feature Matching", function(){
  var feature = new Feature({
    name:"HA11 Chain",
    type:"mat1_peptide",
    range_expression:{
      start:"1/5",
      finish:"21/25"
    },
    motifs:{
      start:"ATG/C",
      finish:"GCC/G"
    }
  });
  feature.adjustBy(-1);
  var sequence1 = new Sequence({
    header:"lcl|H461.1",
    body:"ATG456789012345678GCC"
  });
  var sequence2 = new Sequence({
    header:"lcl|H461.2",
    body:"ATC456789012345678GCG"
  });
  var sequence3 = new Sequence({
    header:"lcl|H461.3",
    body:"ATGCGCCG"
  });

  equal(feature.match(sequence1), true);
  equal(feature.match(sequence2), true);
  deepEqual(feature.matched(), {
    "start": {
      "index": 0,
      "from": 0,
      "to": 2,
      "motif": "ATC"
    },
    "finish": {
      "index": 20,
      "from": 18,
      "to": 20,
      "motif": "GCG"
    },
    "length": 21,
    "range": new Range()
  });
  deepEqual(feature.matched().range.hashify(), {
    start:0,
    finish:20
  });
  equal(feature.match(sequence3), false);

  var sequence4 = new Sequence({
    header:"lcl|H461.3",
    body:"ATC456789012345678GC"
  });
  equal(feature.match(sequence4, 0, 1), true, "matching reducement");
});