test("Feature Manager", function(){
  var featureManager = new FeatureManager();
  var firstFeature = new Feature({
    name:"HA1 Chain",
    type:"mat1_peptide",
    range_expression:{
      start:"0/4",
      finish:"20/24"
    },
    motifs:{
      start:"ATG/C",
      finish:"GCC/G"
    }
  });
  var secondFeature = new Feature({
    name:"HA2 Chain",
    type:"mat1_peptide",
    range_expression:{
      start:"2",
      finish:"20/18"
    },
    motifs:{
      start:"C/G",
      finish:"G"
    }
  });
  featureManager.attach(firstFeature);
  featureManager.attach(secondFeature);

  var sequence1 = new Sequence({
    header:"lcl|H461.1",
    body:"ATG456789012345678GCC"
  });
  var sequence2 = new Sequence({
    header:"lcl|H461.2",
    body:"ATC456789012345678GCG"
  });
  var sequence3 = new Sequence({
    header:"lcl|H461.3",
    body:"ATGCGCCG"
  });

  var sg = new SequenceGroup();
  sg.attach(sequence1);
  sg.attach(sequence2);
  sg.attach(sequence3);
  featureManager.match(sg);

  // First sequence, second feature
  deepEqual(featureManager.findMatched(0)[1].result, {
    "start": {
      "index": 2,
      "from": 2,
      "to": 2,
      "motif": "G"
    },
    "finish": {
      "index": 18,
      "from": 18,
      "to": 18,
      "motif": "G"
    },
    "length": 17,
    "range": new Range()
  }, "First sequence, second feature");
  deepEqual(featureManager.findMatched(0)[1].result.range.hashify(), {
    start:2,
    finish:18
  });
  
  // Second sequence, first feature
  deepEqual(featureManager.findMatched(1)[0].result, {
    "start": {
      "index": 0,
      "from": 0,
      "to": 2,
      "motif": "ATC"
    },
    "finish": {
      "index": 20,
      "from": 18,
      "to": 20,
      "motif": "GCG"
    },
    "length": 21,
    "range": new Range()
  }, "Second sequence, first feature");
  deepEqual(featureManager.findMatched(1)[0].result.range.hashify(), {
    start:0,
    finish:20
  });

  // Second sequence, second feature
  deepEqual(featureManager.findMatched(1)[1].result, {
    "start": {
      "index": 2,
      "from": 2,
      "to": 2,
      "motif": "C"
    },
    "finish": {
      "index": 18,
      "from": 18,
      "to": 18,
      "motif": "G"
    },
    "length": 17,
    "range": new Range()
  }, "Second sequence, second feature");
  deepEqual(featureManager.findMatched(1)[1].result.range.hashify(), {
    start:2,
    finish:18
  });
});