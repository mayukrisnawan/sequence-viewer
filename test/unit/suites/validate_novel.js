test("Validate novel", function(){
  var feature = new Feature({
    "name":"HA2 chain",
    "type":"Feature Type 1",
    "length_expression":"10/11",
    "range_expression":{
      "start":"1/2",
      "finish":"10/12"
    },
    "motifs":{
      "start":"GG A/T",
      "finish":"TGCA"
    },
    "isForValidation":true
  });
  feature.adjustBy(-1);

  var sequenceBody1 = "AGGACAAAAAAAAA";
  var sequenceBody2 = "AGGACCCCTGCA";

  equal(SequenceValidation.novel(sequenceBody1, feature), true);
  equal(SequenceValidation.novel(sequenceBody2, feature), false);
  
});