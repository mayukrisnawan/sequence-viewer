var singleResult;
$.ajax({
  "url":"../resources/fasta_output/single.json",
  "async":false,
  "dataType":"json",
  success:function(response){
    singleResult = response;
  }
});

var multipleResult;
$.ajax({
  "url":"../resources/fasta_output/multiple.json",
  "async":false,
  "dataType":"json",
  success:function(response){
    multipleResult = response;
  }
});

asyncTest("Single Fasta File Parsing", function() {
  $.ajax({
    "url":"../resources/fasta_input/single.fasta",
    "dataType":"text",
    success:function(data){
      var sequence = FastaFileParser.read(data);
      equal(sequence.header(), singleResult.header, "Sequence header should be equals");
      equal(sequence.body(), singleResult.body, "Sequence body should be equals");
      start();
    }
  });
});

asyncTest("Mutiple Fasta File Parsing", function() {
  $.ajax({
    "url":"../resources/fasta_input/multiple.fasta",
    "dataType":"text",
    success:function(data){
      var sequences = FastaFileParser.read(data, true);
      equal(sequences.find(0).header(), multipleResult[0].header, "First sequence header should be equals");
      equal(sequences.find(0).body(), multipleResult[0].body, "First sequence body should be equals");
      equal(sequences.find(3).header(), multipleResult[3].header, "Last sequence header should be equals");
      equal(sequences.find(3).body(), multipleResult[3].body, "Last sequence body should be equals");
      start();
    }
  });
});