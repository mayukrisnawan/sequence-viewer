var banner = [
  "/*",
  "  sequenceViewer.js",
  "  DNA sequence viewer jquery plugin",
  "  Based on IVM (Influenza Virus Monitoring) Online",
  "  author : Mayu Krisnawan",
  "*/\n\n"
].join("\n");

module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.read("package.json"),
    concat: {
      options:{
        banner:banner,
        separator:"\n\n"
      },
      dist: {
        src: [
          'src/open.js',
          'src/utilities/*.js',
          'src/data/*.js',
          'src/ui/UI.js',
          'src/ui/main/*.js',
          'src/init.js',
          'src/templates/*.js',
          'src/events/*.js',
          'src/events/multiple/*.js',
          'src/modules/*.js',
          'src/modules/multiple/*.js',
          'src/SingleSequenceViewer.js',
          'src/MultipleSequenceViewer.js',
          'src/wrapper.js',
          'src/close.js'
        ],
        dest: 'build/sequenceViewer.js'
      }
    },
    copy: {
      main: {
        files : [
          {
            src: 'build/sequenceViewer.js',
            dest: 'C:/rails_apps/ivmonline3.0/vendor/assets/javascripts/sequenceViewer.js',
          },
          {
            src: 'src/css/sv.css',
            dest: 'C:/rails_apps/ivmonline3.0/vendor/assets/stylesheets/sv.css',
          }
        ]
      },
    },
    uglify: {
      options:{
        banner:banner
      },
      sv_script: {
        src: 'build/sequenceViewer.js',
        dest: 'build/sequenceViewer.min.js'
      }
    },
    watch: {
      scripts: {
        files: ['src/*', 'src/*/*', 'src/*/*/*'],
        tasks: ['concat','uglify','copy'],
        options: {
          spawn: false,
        },
      },
    },
    connect: {
      server: {
        options: {
          port: 8000
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.registerTask('default', ['connect', 'watch']);
  grunt.registerTask('build', ['concat', 'uglify']);
}